extends Node

const ALLY = 0
const ENEMY = 1
const NEUTRAL = 2

const GROUP_PLANET = "GROUP_PLANET"

var selected = null setget set_selected
var target_list = [[1,2,3],[0,2,4],[0,1,3,4,5],[0,2,5,6],[1,2,5,7],[2,3,4,6,7,8],[3,5,8,9],[4,5,8,11],[5,6,7,9,10,11],[6,8,10],[8,9,11],[7,8,10]]
var player_enemies_in_range = []
var player_allies_in_range = []

var enemy_allies_in_range = []
# list of enemies in map
var all_planets = []
var enemies = []
var allies = []

signal selected_changed
signal ships_per_second_changed
signal score_changed

func _ready():
	pass

func set_selected(value):
	selected = value
	emit_signal("selected_changed")
	
func get_side_color_name(target):
	var colorname = "white"
	match target:
		global.ALLY:
			colorname = "green"
		global.NEUTRAL:
			colorname = "white"
		global.ENEMY:
			colorname = "red"
	return colorname
	
func manage_planets():
	enemies.clear()
	allies.clear()
	if all_planets.size() == 0:
		return
	for i in all_planets.size():
		if all_planets[i] != null:
			if all_planets[i].side == ALLY:
				allies.append(all_planets[i])
			if all_planets[i].side == ENEMY:
				enemies.append(all_planets[i])
	emit_signal("score_changed")
