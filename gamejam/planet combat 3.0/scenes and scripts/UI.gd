extends Control

func _ready():
	global.connect("selected_changed",self,"manage_buttons")
	global.connect("score_changed",self,"show_score")
	manage_buttons()
	pass

func manage_buttons():
	var active = false
	
	active = global.selected != null
	
	enable_attack(active and (global.player_allies_in_range.size() > 0) and (global.selected.side != global.ALLY))
	enable_support(active and (global.player_allies_in_range.size() > 0) and (global.selected.side == global.ALLY))
	pass
	
func enable_attack(active):
	$Container/btnatk.disabled = !active
	if $Container/btnatk.disabled:
		$Container/lblatk.modulate.r8 = 100
		$Container/lblatk.modulate.g8 = 100
		$Container/lblatk.modulate.b8 = 100
	else:
		$Container/lblatk.modulate.r8 = 255
		$Container/lblatk.modulate.g8 = 255
		$Container/lblatk.modulate.b8 = 255
	pass
	
func enable_support(active):
	$Container/btnsup.disabled = !active
	if $Container/btnsup.disabled:
		$Container/lblsup.modulate.r8 = 100
		$Container/lblsup.modulate.g8 = 100
		$Container/lblsup.modulate.b8 = 100
	else:
		$Container/lblsup.modulate.r8 = 255
		$Container/lblsup.modulate.g8 = 255
		$Container/lblsup.modulate.b8 = 255
	pass
	
func _on_btnatk_pressed():
	global.selected.attack(global.ALLY)
	pass # replace with function body

func _on_btnsup_pressed():
	global.selected.attack(global.ALLY)
	pass # replace with function body

func show_score():
	$Container/lblallies.text = "ALLIES: "+str(global.allies.size())
	$Container/lblenemies.text = "ENEMIES: "+str(global.enemies.size())


