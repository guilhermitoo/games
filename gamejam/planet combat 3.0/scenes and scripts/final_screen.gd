extends Control



func _ready():
	global.connect("score_changed",self,"check_score")
	pass
	
func check_score():
	if global.allies.size() == 0:
		$lbl_victory.text = "YOU LOSE"
		$AnimationPlayer.play("final")
		visible = true
	if global.enemies.size() == 0:
		$lbl_victory.text = "YOU WIN"
		$AnimationPlayer.play("final")
		visible = true


func _on_btn_menu_pressed():
	transition.fade_to("res://scenes and scripts/main_menu.tscn")
	pass # replace with function body
