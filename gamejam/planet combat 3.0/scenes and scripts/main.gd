extends Node2D

# [from,to,side]
var lines = [[Vector2(0,0),Vector2(0,0),1]]

var pulse = 1
var fat_pulse = -1

func _ready():
	set_process(true)
	global.connect("selected_changed",self,"update_lines")
	
	global.all_planets.clear()
	for i in $planets.get_children():
		if i.is_in_group(global.GROUP_PLANET):
			global.all_planets.append(i)
	global.manage_planets()
	pass

func _process(delta):
	var ldelta = delta
	if pulse > 1:
		fat_pulse = -1
	if pulse < 0.06:
		fat_pulse = 1
	if pulse > 0.6:
		ldelta -= (delta *0.4)
	pulse = pulse + (ldelta * fat_pulse)
	update()
	pass
	
func _draw():
	if lines.size() > 0:
		for i in lines.size():
			draw_line(lines[i][0],lines[i][1],get_side_color(lines[i][2]))
		pass
	
func draw_target_lines(source,connected):
	global.player_enemies_in_range.clear()
	global.player_allies_in_range.clear()
	var spos = get_node("planets/"+str(source)).position
	var cpos = Vector2(0,0)
	var cside = -1
	var sside = get_node("planets/"+str(source)).side
	for i in range(connected.size()):
		cpos = get_node("planets/"+str(connected[i])).position
		cside = get_node("planets/"+str(connected[i])).side
		if (cside != global.ALLY):
			global.player_enemies_in_range.append(get_node("planets/"+str(connected[i])))
		if (cside == global.ALLY):
			global.player_allies_in_range.append(get_node("planets/"+str(connected[i])))
		new_line(spos,cpos,cside)
	pass
	
	
func new_line(from,to,side):
	lines.append([from,to,side])


func update_lines():
	lines.clear()
	if global.selected != null:
		draw_target_lines(global.selected.name,global.target_list[int(global.selected.name)])
		

func get_side_color(target):
	var color = ColorN("white",1)
	match target:
		global.ALLY:
			color = ColorN("green",pulse)
		global.NEUTRAL:
			color = ColorN("white",1)
		global.ENEMY:
			color = ColorN("red",pulse)
	return color


func _on_Timer_timeout():
	enemy_play()
	pass # replace with function body

func enemy_play():
	
	global.enemy_allies_in_range.clear()
	
	if global.allies.size() <= 0:
		return
	
	var inrange = []
	for e in global.enemies.size():
		inrange = global.target_list[int(global.enemies[e].name)]
		for x in inrange.size():
			if get_node("planets/"+str(inrange[x])).side != global.ENEMY:
				global.enemy_allies_in_range.append(global.enemies[e])
				get_node("planets/"+str(inrange[x])).attack(global.ENEMY)
	

