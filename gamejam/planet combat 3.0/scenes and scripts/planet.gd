extends Area2D

enum e_side {ALLY,ENEMY,NEUTRAL}

var planet_visuals = ["res://resources/p1.png","res://resources/p2.png","res://resources/p3.png","res://resources/p4.png"]
export var fixed_size = 0
export(e_side) var side = global.NEUTRAL setget set_side

var ships = 0 setget set_ships
var sending_ships = 0
var sending_target = Vector2(0,0)
var generation_factor = 1
var production_slower_factor = 0.02
var pre_ship = preload("res://scenes and scripts/ship.tscn")
var last_attack_side = 0

onready var sprite = get_node("sprite")
onready var circle = get_node("circle")

func _ready():
	set_start_sprite()
	set_start_scale()
	set_generation_factor()
	set_process(true)
	add_to_group(global.GROUP_PLANET)
	
	global.connect("selected_changed",self,"verify_selected_planet")
	pass 
	
func set_start_scale():
	var i = 0
	if fixed_size == 0:
		i = rand_range(1,3)
	else:
		i = fixed_size
	scale.x = i
	scale.y = i

func set_start_sprite():
	var i = 0
	randomize()
	i = randi() % planet_visuals.size()
	var spr = load(planet_visuals[i])
	if find_node("sprite"):
		sprite.texture = spr
		
func _process(delta):
	if sending_ships > 0:
		send_one_ship()
	pass
	
func set_generation_factor():
	var i = 1
	if side == global.ALLY:
		i = 2.2
	if side == global.ENEMY:
		i = 2.5
	if i < 2:
		var x = ( production_slower_factor * 10-ships)
		if x > 0:
			i += x
	generation_factor = ((i * scale.x) + i)/20
	
func set_ships(value):
	ships = value
	set_generation_factor()
	$lbl_ships.text = str(int(ships))
	
	if ships <= 0:
		set_side(last_attack_side)
		set_generation_factor()

func _on_Timer_timeout():
	set_ships(generation_factor + ships)

func select_planet():
	global.set_selected(self)
	pass
	
func verify_selected_planet():
	if global.selected != self:
		circle.hide()
	else:
		circle.show()
	
func set_side(value):
	side = value
	if side == ALLY:
		#pinta de verde
		$circle.modulate = ColorN("green",1)
		if has_node("sprite") :
			$sprite.self_modulate = ColorN("green",1)
		$audio_ally.play()
	elif side == ENEMY:
		#pinta de vermelho
		$circle.modulate = ColorN("red",1)
		if has_node("sprite") :
			$sprite.self_modulate = ColorN("red",1)
		$audio_enemy.play()
	else:
		if has_node("sprite/circle"):
			$circle.modulate = ColorN("white",1)
		if has_node("sprite") :
			$sprite.self_modulate = ColorN("white",1)
	global.set_selected(global.selected)
	global.manage_planets()
	
	
func _on_planet_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
	and event.button_index == BUTTON_LEFT \
	and event.is_pressed():
		select_planet()
	
func attack(side_attacking):
	var planets_attk = []
	if side_attacking == global.ALLY:
		# receive attack from green planets connected
		planets_attk = global.player_allies_in_range
	else:
		# receive attack from red planets connected
		planets_attk = global.enemy_allies_in_range
	
	for i in planets_attk.size(): 
		planets_attk[i].send_ships_to_target(get_variation_position())
	
func send_ships_to_target(target):
	if ships > 2:
		sending_ships = int(ships)/2
		ships -= sending_ships
		sending_target = target
	
func send_one_ship():
	var s = pre_ship.instance()
	s.side = side
	s.owner_name = name
	s.position = get_variation_position()
	get_node("../").add_child(s)
	s.go_to(sending_target)
	sending_ships -= 1
	
func get_variation_position():
	var pv = self.position
	var variation = Vector2(0,0)
	var v_f = -15
	var v_t = 15
	
	randomize()
	variation.x = int(rand_range(v_f,v_t))
	randomize()
	variation.y = int(rand_range(v_f,v_t))
	
	pv += variation
	
	return pv
	
func _on_planet_body_entered(body):
	if body.side != self.side:
		last_attack_side = body.side
		set_ships(ships-1)
		body.queue_free()
	else:
		if body.owner_name != name:
			set_ships(ships+1)
			body.queue_free()
	pass # replace with function body
