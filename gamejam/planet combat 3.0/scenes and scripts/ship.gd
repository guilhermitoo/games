extends KinematicBody2D

var speed = 12
var destination = Vector2(0,0)
var side
var owner_name = ""

func _ready():
	set_process(true)
	pass

func _process(delta):
	if destination == Vector2(0,0):
		return
	
	var motion = destination * speed * delta 
	move_and_slide(motion)
	motion = Vector2(0,0)
	pass
	
func go_to(target):
	destination = target-position
	$ship.self_modulate = ColorN(global.get_side_color_name(side),1)
	self.look_at(target)
	pass
	
	
	
func _on_Timer_timeout():
	queue_free()
	pass # replace with function body
