extends Area2D

const ALLY = 0
const ENEMY = 1
const NEUTRAL = 2

enum e_side {ALLY,ENEMY,NEUTRAL}
var planet_visuals = ["res://sprites/p1.png","res://sprites/p2.png","res://sprites/p3.png","res://sprites/p4.png"]
export var fixed_size = 0
export(e_side) var side = NEUTRAL

var ships = 0 setget set_ships

var generation_factor = 1

func _ready():
	set_start_sprite()
	set_start_scale()
	set_generation_factor()
	set_process(true)
	pass 
	
func set_start_scale():
	var i = 0
	if fixed_size == 0:
		i = rand_range(1,3)
	else:
		i = fixed_size
	scale.x = i
	scale.y = i

func set_start_sprite():
	var i = 0
	randomize()
	i = randi() % planet_visuals.size()
	var spr = load(planet_visuals[i])
	if find_node("sprite"):
		get_node("sprite").texture_normal = spr
		
func _process(delta):
	pass
	
func set_generation_factor():
	var i = 1
	if side in [ALLY,ENEMY]:
		i = 2.5
	generation_factor = ((i * scale.x) + i)/10
	
func set_ships(value):
	ships = value
	$lbl_ships.text = str(int(ships))

func _on_Timer_timeout():
	set_ships(generation_factor + ships)
	pass # Replace with function body.


func _on_TextureButton_pressed():
	print("clicou em : " + name)
	pass # Replace with function body.
