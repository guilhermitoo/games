extends RigidBody2D

export var speed = 10

func _ready():
	set_process(true)
	pass

func _process(delta):
	get_parent().offset += delta*speed
	pass
