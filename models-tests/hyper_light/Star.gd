extends Node2D

var x
var y
var z
var sx
var sy
var cx
var cy
var spread
var scl
var px
var py

onready var width  = OS.get_real_window_size().x
onready var height = OS.get_real_window_size().y

var speed = 0.5

func _ready():
	set_position()
	
	set_process(true)
	pass
	
func set_position():
	randomize()
	x = rand_range(-width,width)
	y = rand_range(-height,height)
	sx = x
	sy = y
	px = 0
	py = 0
	z = width/4
	cx = width/2
	cy = height/2
	scl = 0.5
	spread = width/4
func _draw():
	draw_circle(Vector2(sx,sy),scl,Color(255,255,255))
	if px!=0:
		draw_line(Vector2(px,py), Vector2(sx,sy), Color(255,255,255), 0.5)
	pass

func _process(delta):
	if speed < 5:
		speed += delta/2
#	else:
#		scl += delta
	z -= speed
	if sx<0 or sx > width:
		set_position()
	if sy<0 or sy > height:
		set_position()
	if z<=0:
		set_position()
	px = sx
	py = sy
	sx = (x*spread)/(z)+cx
	sy = (y*spread)/(z)+cy
	update()
	pass
