extends Node


# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var pos = Vector2( )
var pre_tiro = preload("res://tiro.tscn");
var delay = 0;
var speed = 1000
var rotation_speed = 10

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	pass	

func _process(delta):
	get_node("Area2D/Sprite").look_at(get_node("Area2D").get_global_mouse_pos())
	#print(get_node("Area2D/Sprite").get_rotd())
	var character = get_node("Area2D")
	var mouseglobal = get_node("Area2D").get_global_mouse_pos()
	var mouselocal = get_node("Area2D").get_local_mouse_pos()
	
	# MOVIMENTAÇÃO
	if Input.is_action_pressed("ui_up"):
		character.translate(Vector2(0,-1)*delta*speed)
	if Input.is_action_pressed("ui_down"):
		character.translate(Vector2(0,1)*delta*speed)
	if Input.is_action_pressed("ui_left"):
		character.translate(Vector2(-1,0)*delta*speed)
	if Input.is_action_pressed("ui_right"):
		character.translate(Vector2(1,0)*delta*speed)
	
	if Input.is_action_pressed("shoot"):
		delay -= delta;
		if delay <= 0:
			var tiro = pre_tiro.instance()
			tiro.set_pos(character.get_global_pos())
			tiro.look_at(mouseglobal)
			
			game.getMain().add_child(tiro)
			tiro.mousepos = mouselocal
			delay = 0.1;
		
	MoveRay(delta)
	
func MoveComand():
	return Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down")
	pass
	
func MoveRay(delta):
	if MoveComand():
		var ray_node = get_node("KinematicBody2D/RayCast2D")
		var rotation_final
		var dir = Vector2()
		var rotd = ray_node.get_rotd()
		#negativo = sentido horário
		#positivo = sentido anti-horário
		var nv = 1
		
		if Input.is_action_pressed("ui_right"):
			if rotd > 90:
				nv = -1
			else:
				nv = 1
		if Input.is_action_pressed("ui_up"):
			if (rotd < 180) and (rotd > 0):
				nv = 1
			else:
				nv = -1
			
		if Input.is_action_pressed("ui_left"):
			if rotd > -90:
				nv = 1
			else:
				nv = -1
		if Input.is_action_pressed("ui_down"):
			if rotd > 0:
				nv = -1
			else:
				nv = 1
		
		
		ray_node.rotate(nv*delta*rotation_speed)
		#print(ray_node.get_rotd())
#		ray_node.look_at(dir)
#		nv = GetNearestValue(rotd,rotation_final)
#		if nv < rotd:
#			rotd -= delta * rotation_speed
#		elif nv > rotd:
#			rotd += delta * rotation_speed 
#			
#		print(nv)
#		print(rotd)
#		if nv == round(rotd):
#			rotd = nv
#			
#		ray_node.set_rotd(rotd)
	
	pass
	
func GetNearestValue( value, arr ):
	var res = 999999
	var i = 0
	var c = 0
	
	while i < arr.size():
		if RemNeg(arr[i]) < RemNeg(value):
			c = RemNeg(value) - RemNeg(arr[i])
		else:
			c = RemNeg(arr[i]) - RemNeg(value)
		if c < res:
			res = arr[i]
		i += 1
	return res
	
func RemNeg(value):
	if value < 0:
		return value * (-1)
	else:
		return value