extends CanvasLayer

func _ready():
	CarregarHighscores()
	
	game.alignGlobalCenter(get_node("Button"))
	game.alignGlobalCenter(get_node("GridContainer"))
	game.alignGlobalCenter(get_node("Label"))
	
func CriarLabel(text):
	var label = Label.new()
	label.set_text(text)
	label.add_font_override("font",load("res://fonts/score.fnt"))
	
	get_node("GridContainer").add_child(label)


func _on_Button_button_down():
	get_tree().change_scene("res://scenes/inicio.tscn")
	pass # replace with function body

func CarregarHighscores():
	CriarLabel("17-02-2018 - 250000")
	CriarLabel("18-02-2018 - 230111")
	CriarLabel("19-02-2018 - 200232")