extends Node

const GRUPO_INIMIGO = "inimigos"
const GRUPO_TIRO_INIMIGO = "GTI"
const GRUPO_NAVE = "nave"
const GRUPO_SHIELD = "shield"

var score = 0 setget setScore
var pontuacao_por_meteoro = 10
var vel_background = 300 # escala que multiplica a velocidade do fundo

# PROPRIEDADE PARA CONTAR A VIDA
var nave_vida = 0 setget setVidas
var nave_velocidade = 460
var nave_enemy_vel = 200
# HP INICIAL DA NAVE
var nave_hp = 6

# PROPRIEDADE PARA CONTAR VIDA ESCUDO
var escudo_vida = 0 setget setVidaEscudo

var chance_bonus = 100 # %
# libera os objetos que passarem da margem da tela + margem_acrescida_tela
var margem_acrescida_tela = 50
var jogo_rodando = true

# METEOR PROPERTIES
var meteor_interval_ini
var meteor_interval_fim
var meteor_vel_ini
var meteor_vel_fim
var meteor_rot_ini
var meteor_rot_fim 
var meteor_scale_ini 
var meteor_scale_fim 

var meteor_hp = 2 # HITPOINTS # VIDA

# SHOOT PROPERTIES
var shoot_vel = 800
var shoot_dano = 1
var shoot_municao_padrao = 40

# POWER UP PROPERTIES
var pu_interval_ini = 20
var pu_interval_fim = 60

signal score_changed
signal lifes_changed
signal shield_changed

func _ready():
	setVidas(nave_hp)
	iniciarControles()
	randomize()
	pass
	
func setVidaEscudo(valor):
	escudo_vida = valor
	emit_signal("shield_changed")
	pass
	
func getCamera():
	return getMain().get_node("camera")	
	pass
	
func getMain():
	return get_tree().get_root().get_node("main")
	pass
	
func setScore(valor):
	if valor > 0:
		score = valor
		emit_signal("score_changed")
		var delta = valor*get_process_delta_time()
		atualizarControles(delta)
	pass
	
func setVidas(valor):
	nave_vida = valor
	emit_signal("lifes_changed")
	pass
	
func estaForaTela(node):
	var retorno = false
	var tamanho_tela = OS.get_window_size()
	var local_no = node.get_global_pos()
	retorno = retorno or ( local_no.x > (tamanho_tela.x+margem_acrescida_tela) )
	retorno = retorno or ( local_no.x < -margem_acrescida_tela ) 
	retorno = retorno or ( local_no.y > (tamanho_tela.y+margem_acrescida_tela) ) 
	retorno = retorno or ( local_no.y < -margem_acrescida_tela )
	return retorno
	pass
	
func pausar():
	if jogo_rodando:
		get_tree().set_pause( ! get_tree().is_paused())  
		var x = getMain().get_node("HUD").get_node("pause_menu")
		x.set_hidden(!x.is_hidden())
	pass
	
func sair():
	get_tree().quit()
	pass
	
func fimDeJogo():
	# termina o jogo
	save_highscores()
	jogo_rodando = false
	get_tree().set_pause(true)
	var x = getMain().get_node("HUD").get_node("game_over")
	x.set_hidden(false)
	pass
	
func alignGlobalCenter(node):
	var osx = Globals.get("display/width")
	
	
	node.set_global_pos(Vector2(((osx*0.5)-(node.get_size().x*0.5)),node.get_global_pos().y))
	pass
	
func atualizarControles(delta):
# AUMENTA A VELOCIDADE DOS METEOROS, COM RELAÇÃO AO SCORE E A VELOCIDADE DA CAMERA
	meteor_vel_ini = vel_background * 0.7
	meteor_vel_fim = vel_background
	# AUMENTA A FREQUÊNCIA DE SPAWN DE METEOROS, COM RELAÇÃO A SCORE
	if meteor_interval_ini > 0:
		meteor_interval_ini -= (delta * 0.5)
	if meteor_interval_fim > 0.5:
		meteor_interval_fim -= delta
		
	# AUMENTA A VELOCIDADE DA CÂMERA COM RELAÇÃO AO SCORE
	vel_background += delta*50
	pass
	
func iniciarControles():
	meteor_vel_ini = vel_background * 0.7
	meteor_vel_fim = vel_background
	meteor_interval_ini = 0.2
	meteor_interval_fim = 1.5
	meteor_rot_ini = -50
	meteor_rot_fim = 50
	meteor_scale_ini = 0.7
	meteor_scale_fim = 1.8
	pass
	
func save_highscores():
    var save_game = File.new()
    save_game.open("user://highscores.save", File.WRITE)
    var save_nodes = get_tree().get_nodes_in_group("Persist")
    for i in save_nodes:
        var node_data = i.getfiletype()
        save_game.store_line(to_json(node_data))
    save_game.close()

func gethighscore():
    var highscores = {
        date = OS.get_datetime(false), highscore = score
    }
    return highscores

func load_highscores():
    var save_game = File.new()
    if not save_game.file_exists("user://highscores.save"):
        return # Error!  We don't have a save to load.

    # We need to revert the game state so we're not cloning objects during loading.  This will vary wildly depending on the needs of a project, so take care with this step.
    # For our example, we will accomplish this by deleting savable objects.
    var save_nodes = get_tree().get_nodes_in_group("Persist")
    for i in save_nodes:
        i.queue_free()

    # Load the file line by line and process that dictionary to restore the object it represents
    save_game.open("user://savegame.save", File.READ)
    while not save_game.eof_reached():
        var current_line = parse_json(save_game.get_line())
        # First we need to create the object and add it to the tree and set its position.
        var new_object = load(current_line["filename"]).instance()
        get_node(current_line["parent"]).add_child(new_object)
        new_object.set_position(Vector2(current_line["pos_x"],current_line["pos_y"]))
        # Now we set the remaining variables.
        for i in current_line.keys():
            if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
                continue
            new_object.set(i, current_line[i])
    save_game.close()