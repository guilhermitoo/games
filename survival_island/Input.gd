extends Node

func _ready():
	set_process(true)
	pass 
		
func _process(delta):
	if Input.is_action_pressed("ui_right"):
		$"../".go_right()
	elif Input.is_action_pressed("ui_left"):
		$"../".go_left()
	elif Input.is_action_pressed("ui_up"):
		$"../".go_up()
	elif Input.is_action_pressed("ui_down"):
		$"../".go_down()
	pass
