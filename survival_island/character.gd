extends KinematicBody2D

var left = Vector2(-1,0)
var right = Vector2(1,0)
var up = Vector2(0,-1)
var down = Vector2(0,1)

var move = Vector2(0,0)
var anim = ""

var speed = 2
var speed_fact = 40

var active_anim

func _ready():
	set_process(true)
	pass

func go_right():
	move = right
	anim = "walk_right"
	$right.visible = true
	$left.visible = false
	$up.visible = false
	$down.visible = false
	
func go_left():
	move = left
	anim = "walk_left"
	$right.visible = false
	$left.visible = true
	$up.visible = false
	$down.visible = false

func go_up():
	move = up
	anim = "walk_up"
	$right.visible = false
	$left.visible = false
	$up.visible = true
	$down.visible = false
	
func go_down():
	move = down
	anim = "walk_down"
	$right.visible = false
	$left.visible = false
	$up.visible = false
	$down.visible = true

func _process(delta):
	if anim != "":
		if ! $anim.is_playing() or anim != active_anim:
			$anim.play(anim)
			active_anim = anim
	else:
		active_anim = ""
		$anim.stop()
		$right.region_rect.position.x = 0
		$left.region_rect.position.x = 105
		$up.region_rect.position.x = 0
		$down.region_rect.position.x = 0
		
	var a = move*speed*speed_fact
	self.move_and_slide(a)
	
	move = Vector2(0,0)
	anim = ""
	
	pass
