extends StaticBody2D

var hitpoints = 10

func _ready():
	set_process(true)
	add_to_group(game.GROUP_MAP_OBJECT)
	pass
	
func DealDamage(damage):
	hitpoints -= damage
	print("hp:"+str(hitpoints))
	if hitpoints <= 0:
		_destroy()
	pass
	
func _destroy():
	queue_free()
	pass