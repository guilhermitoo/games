extends Area2D

var pre_mun
var mun
var owner setget setOwner
var shoot_frequency = 1 #tiros por segundo
var bullet_leave

#propriedades de controle
var interval = 0

func _ready():
	set_process(true)
	pass
	
func _process(delta):
	MouseSight(delta)
	if Input.is_action_pressed("ui_shoot"):
		Shoot(game.getTarget()) #owner.get_local_mouse_pos()
	interval -= delta
	
func setOwner(value):
	owner = value
	
func MouseSight(delta):
	# faz o canhão mirar o mouse
	get_node(".").look_at(game.getTarget().get_global_pos())#get_node(".").get_global_mouse_pos())
	pass
	
func Shoot(target):
	if interval <= 0:
		# função instancia um projetil e atira no alvo(target)
		# INSTANCIA O TIRO A PARTIR DO pre_mun
		var mun = pre_mun.instance()
		# DEFINE A POSIÇÃO DELE NA POSIÇÃO DA ARMA
		if has_node("bullet_leave_position_2"):
			if bullet_leave != get_node("bullet_leave_position_2"):
				bullet_leave = get_node("bullet_leave_position_2")
			else:
				bullet_leave = get_node("bullet_leave_position")
		else:
				bullet_leave = get_node("bullet_leave_position")
		mun.set_pos(bullet_leave.get_global_pos())
		mun.shooter = owner
		game.getMain().add_child(mun)
		mun.look_at(target.get_global_pos())
		var local_pos = target.get_global_pos()
		local_pos = local_pos - self.get_global_pos()
		mun.target = local_pos
		interval = shoot_frequency
	pass

func _on_timer_timeout():
	get_node("timer").stop()
	interval = 0
	pass # replace with function body
