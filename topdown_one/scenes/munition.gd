extends RigidBody2D

var shooter setget setShooter #recebe o veiculo que atirou
var damage = 10
var wr
var target = Vector2(0,0)
var final_speed = 2500

var is_rocket = false
var acceleration = 1200
var speed = 1000

func _ready():
	set_process(true)
	pass
	
func _process(delta):
	if target != Vector2(0,0):
		if ! is_rocket:
			speed = final_speed
		else:
			speed += delta * acceleration
			speed = clamp(speed, 0, final_speed)
		var a = (target.normalized())*speed
		set_linear_velocity(a)
	# fica verificando com uma referencia fraca se o objeto creator está atribuido
	wr = weakref(shooter)
	pass
	
func BodyEnter( area ):
	print(area)
	# se tem referência, ou seja, se está atribuido, então executa
	if wr && wr.has_method("get_ref"):
		if wr.get_ref():
			if area != shooter:
				if area.has_method("DealDamage"):
					area.DealDamage(damage)
					FreeInstance()
	pass 
	
func setShooter(value):
	shooter = value
	pass
	
func FreeInstance():
	print("munition.free")
	queue_free()
	
func _on_timer_timeout():
	FreeInstance()
	pass

func _on_munition_body_enter( body ):
	BodyEnter(body)
	pass # replace with function body
