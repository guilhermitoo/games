extends KinematicBody2D

var direction = Vector2()
var speed = 0
var acceleration = 1500
var decceleration = 100

var motion = Vector2()
var target_motion = Vector2()
var steering = Vector2()
const MASS = 10

const final_speed = 200

var target_angle = 0
var hitpoints = 350
var ray_node

func _ready():
	set_fixed_process(true)
	add_to_group(game.GROUP_TANK)
	
	ray_node = get_node("RayCast2D")
	pass
	
func _fixed_process(delta):
	
	MoveTank(delta,game.DRIVE_MODE)
	pass
	
func MoveTank(delta,drive_mode):
	direction = Vector2()
	
	if drive_mode == game.DRIVE_MODE_DIRECTION:
		direction = game.getDirectionInput()
		
		if direction != Vector2():
			speed += acceleration * delta
		else:
			speed -= decceleration * delta
		
		speed = clamp(speed, 0, final_speed)
		
		target_motion = speed * direction.normalized() * delta
		steering = target_motion - motion
	
		if steering.length() > 1:
			steering = steering.normalized()
		
		motion += steering / MASS
	
		if speed == 0:
			motion = Vector2()
	
		move(motion)
		if motion != Vector2():
			target_angle = atan2(motion.x, motion.y)# - PI/2
			ray_node.set_rot(target_angle)
		
	pass
	
func _destroy():
	remove_from_group(game.GROUP_TANK)
	queue_free()
	pass
	
func DealDamage(damage):
	hitpoints -= damage
	print("hp:"+str(hitpoints))
	if hitpoints <= 0:
		_destroy()
	pass