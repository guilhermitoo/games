extends Node

const GROUP_TANK = 'TANKS'
const GROUP_MAP_OBJECT = 'MAPOBJECT'
var   DRIVE_MODE_DIRECTION = 'DIRECTION'

# padrao DRIVE_MODE_DIRECTION
var DRIVE_MODE = DRIVE_MODE_DIRECTION

func _ready():
	#esconde o mouse para aparecer a mira no lugar
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	pass

func getMain():
	return get_tree().get_root().get_node("main")
	pass
	
func getTarget():
	return getMain().get_node("target")
	pass
	
func getDirectionInput():
	var direction = Vector2(0,0)
	
	if Input.is_action_pressed("ui_up"):
		direction.y = -1
	elif Input.is_action_pressed("ui_down"):
		direction.y = 1
	if Input.is_action_pressed("ui_left"):
		direction.x = -1
	elif Input.is_action_pressed("ui_right"):
		direction.x = 1
		
	return direction
	pass
	
func RemNeg(value):
	#remove o negativo
	if value < 0:
		return value * (-1)
	else:
		return value

func Subtr( n1,n2 ):
	var res
	#subtrai o maior numero do menor numero
	if n1 > n2:
		res = n1 - n2
	else:
		res = n2 - n1
	return res
	pass
	
func Sum( ActualN, SumN, MaxN ):
	var res = 0
	res = ActualN + SumN
	if res > MaxN:
		res = MaxN
	return res
	
func Sub( ActualN, SubN, MinN ):
	var res = 0
	res = ActualN - SubN
	if res < MinN:
		res = MinN
	return res
	
func GetInverse( vector ):
	# 1,0  inverse -1,0
	# 0,1  inverse 0,-1
	# 1,1  inverse -1,-1
	# -1,1 inverse 1,-1
	var res = vector
	if res.x != 0:
		res.x = res.x * (-1)
	if res.y != 0:
		res.y = res.y * (-1)
	return res
	
	