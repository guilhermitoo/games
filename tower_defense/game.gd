extends Node2D

var selection = null setget set_selection

func _ready():
	set_process(true)
	
func _process(delta):
	update()
	
func set_selection(value):
	selection = value
	
func _draw():
	if selection != null:
		var center = selection.position+Vector2(0,-40)
		var color = Color(1.0, 0.0, 0.0)
		draw_line(selection.position,center,color,3)
	
func _input(event):
	if event is InputEventMouseButton:
		if selection != null:
			selection.set_target(get_global_mouse_position()-selection.position)
	
	
	



