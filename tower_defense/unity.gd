extends KinematicBody2D

var target = Vector2(0,0) setget set_target
const RADIUS = 32
var speed = 100

func _ready():
	set_physics_process(true)
	set_process_input(true)
	pass
	
func _physics_process(delta):
	var bodies = $Area2D.get_overlapping_bodies()
	
	if bodies.size() != 0:
		var b = bodies[0]
		
		look_at(b.position)
		
	
	if target != Vector2(0,0):
		move_and_slide(target.normalized()*speed)
		target -= target.normalized()
		print(str(target.distance_to(position)))
		if target.distance_to(Vector2(0,0)) < 1:
			target = Vector2(0,0)
	
	print("position: "+str(position))
	print("target: "+str(target))
		
	
	
func _input(event):
	if event is InputEventMouseButton:
		if position.distance_to(event.position) < RADIUS:
			get_node("..").set_selection(self)
	
func set_target(value):
	target = value
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
