extends Node

var modo_teste = false
var advertising = false

var pictures = [{"name" : "Cleber", "path" : "res://sprites/Portraits/M (1).png", "in_use" : false},
				{"name" : "Deiv", "path" : "res://sprites/Portraits/M (2).png", "in_use" : false},
				{"name" : "William", "path" : "res://sprites/Portraits/M (3).png", "in_use" : false},
				{"name" : "Claudio", "path" : "res://sprites/Portraits/M (4).png", "in_use" : false},
				{"name" : "Silvio", "path" : "res://sprites/Portraits/M (5).png", "in_use" : false},
				{"name" : "Emílio", "path" : "res://sprites/Portraits/M (6).png", "in_use" : false},
				{"name" : "Leandro", "path" : "res://sprites/Portraits/M (7).png", "in_use" : false},
				{"name" : "Fernando", "path" : "res://sprites/Portraits/M (8).png", "in_use" : false},
				{"name" : "Raimundo", "path" : "res://sprites/Portraits/M (9).png", "in_use" : false},
				{"name" : "Lavinio", "path" : "res://sprites/Portraits/M (10).png", "in_use" : false},
				{"name" : "Janaina", "path" : "res://sprites/Portraits/F (1).png", "in_use" : false},
				{"name" : "Mariana", "path" : "res://sprites/Portraits/F (2).png", "in_use" : false},
				{"name" : "Janíce", "path" : "res://sprites/Portraits/F (3).png", "in_use" : false},
				{"name" : "Cremilda", "path" : "res://sprites/Portraits/F (4).png", "in_use" : false},
				{"name" : "Rogéria", "path" : "res://sprites/Portraits/F (5).png", "in_use" : false},
				{"name" : "Creuza", "path" : "res://sprites/Portraits/F (6).png", "in_use" : false},
				{"name" : "Hannah", "path" : "res://sprites/Portraits/F (7).png", "in_use" : false},
				{"name" : "Magda", "path" : "res://sprites/Portraits/F (8).png", "in_use" : false},
				{"name" : "Ílce", "path" : "res://sprites/Portraits/F (9).png", "in_use" : false},
				{"name" : "Olga", "path" : "res://sprites/Portraits/F (10).png", "in_use" : false}]

var tipo_baralho = 1
#4=1
#5=2
#6=3
#7=4
#8=5
#9=6
#10=7
#Q=8
#J=9
#K=10
#A=11
#2=12
#3=13
var vira = 0 setget set_vira

var nome_player_turno_atual setget set_player_turno_atual
var nome_player
var vencedor_primeira_rodada = 0 # 1 para time impar e 2 para time par
# número que controla quantas jogadas foram feitas na rodada
var numero_turno = 0 setget set_numero_turno # de 0 a 3 turnos obrigatórios
var numero_prox_turno = 0 # de 0 a 3, sempre 1 a mais que o numero_turno, resetando caso ultrapasse o maximo
var numero_rodada = 1 # de 1 a 3 possíveis rodadas
var numero_partida = 0 # de 1 a 23 possíveis partidas
var rodada_desempate = false

var pontuacao_geral_time_par = 0 setget set_geral_tpar, get_geral_tpar
var pontuacao_geral_time_impar = 0 setget set_geral_timpar, get_geral_timpar
var pontuacao_parcial_time_par = 0 setget set_parcial_tpar, get_parcial_tpar
var pontuacao_parcial_time_impar = 0 setget set_parcial_timpar, get_parcial_timpar

var valor_partida = 1 setget set_valor_partida
var valor_partida_solicitado = 3
var tempo_duracao_turno = 30.0 # segundos para realizar uma jogada. Caso o tempo acabe, uma carta aleatória será jogada de sua mão

var time_trucou = 0 # 1 para time impar e 2 para time par
var analise_truco = false # true para quando o jogo está em fase de análise de truco

var considerar_naipe = false
var texto_tela = ""

var chance_jogar_quieto = 6.5
var chance_blefe = 4
var chance_jogar_escondido = 30
var placar_final = 12


signal exibir_texto_tela
signal fim_do_turno
signal fim_do_turno_sem_jogada
signal fim_da_rodada
signal fim_da_partida
signal fim_do_jogo

signal placar_geral_mudou
signal placar_parcial_mudou
signal turno_mudou
signal vira_feita
signal truco
signal valor_partida_mudou
signal truco_confirmado

func _ready():
	pass
	
func set_vira(value):
	vira = value
	emit_signal("vira_feita")
	
func finalizar_turno():
	if numero_turno >= 3:
		emit_signal("fim_da_rodada")
	else:
		set_numero_turno(numero_prox_turno)
		emit_signal("fim_do_turno")
	
func finalizar_partida():
	if pontuacao_parcial_time_par >= 2:
		set_geral_tpar(get_geral_tpar()+valor_partida)
		emit_signal("fim_da_partida")
	elif pontuacao_parcial_time_impar >= 2:
		set_geral_timpar(get_geral_timpar()+valor_partida)
		emit_signal("fim_da_partida")
	
func set_parcial_tpar(value):
	pontuacao_parcial_time_par = value
	emit_signal("placar_parcial_mudou")
	print("parcial par: " + str(get_parcial_tpar()))

func set_parcial_timpar(value):
	pontuacao_parcial_time_impar = value
	emit_signal("placar_parcial_mudou")
	print("parcial impar: " + str(get_parcial_timpar()))
	
func set_geral_tpar(value):
	pontuacao_geral_time_par = value
	emit_signal("placar_geral_mudou")
	print("geral par: " + str(get_geral_tpar()))
	if get_geral_tpar() >= placar_final:
		emit_signal("fim_do_jogo")
	
func set_geral_timpar(value):
	pontuacao_geral_time_impar = value
	emit_signal("placar_geral_mudou")
	print("geral impar: " + str(get_geral_timpar()))
	if get_geral_timpar() >= placar_final:
		emit_signal("fim_do_jogo")
	
func get_geral_tpar():
	return pontuacao_geral_time_par
	
func get_geral_timpar():
	return pontuacao_geral_time_impar
	
func get_parcial_tpar():
	return pontuacao_parcial_time_par
	
func get_parcial_timpar():
	return pontuacao_parcial_time_impar
	
func set_player_turno_atual(value):
	nome_player_turno_atual = value
	emit_signal("turno_mudou")
	
func truco(time):
	analise_truco = true
	time_trucou = time
	if valor_partida == 1:
		valor_partida_solicitado = 3
	else:
		valor_partida_solicitado = valor_partida+3
		
	print("time "+str(time)+" pediu "+str(valor_partida_solicitado)+"!")
	emit_signal("truco")
	
func confirmar_truco():
	analise_truco = false
	set_valor_partida(valor_partida_solicitado)
	emit_signal("truco_confirmado")
	
func aumentar_truco(time):
	set_valor_partida(valor_partida_solicitado)
	truco(time)
	
func fugir(time):
	print("time "+str(time)+" fugiu!")
	if time == 2:
		set_parcial_timpar(2)
		finalizar_partida()
	elif time == 1:
		set_parcial_tpar(2)
		finalizar_partida()
	
func set_valor_partida(value):
	valor_partida = value
	emit_signal("valor_partida_mudou")
	
	
func set_numero_turno(value):
	numero_turno = value
	numero_prox_turno = (numero_turno+1)%4
	
func exibirTexto(texto):
	texto_tela = texto
	emit_signal("exibir_texto_tela")
	
func textoProxTruco(vlr):
	var bt = "Truco! "
	if vlr == 3:
		bt = "Seis! "
	elif vlr == 6:
		bt = "NOVE! "
	elif vlr == 9:
		bt = "DOZE! "
	return bt
	
func iniciarVariaveis():
	vira = 0
	vencedor_primeira_rodada = 0
	numero_turno = 0 
	numero_prox_turno = 0 # de 0 a 3, sempre 1 a mais que o numero_turno, resetando caso ultrapasse o maximo
	numero_rodada = 1 # de 1 a 3 possíveis rodadas
	numero_partida = 0 # de 1 a 23 possíveis partidas
	rodada_desempate = false
	pontuacao_geral_time_par = 0
	pontuacao_geral_time_impar = 0
	pontuacao_parcial_time_par = 0
	pontuacao_parcial_time_impar = 0
	valor_partida = 1 
	valor_partida_solicitado = 3
	time_trucou = 0 # 1 para time impar e 2 para time par
	analise_truco = false # true para quando o jogo está em fase de análise de truco
	considerar_naipe = false
	texto_tela = ''
	
	
func timeDeOnze():
	var time_retorno = 0
	if (pontuacao_geral_time_impar == 11) and (pontuacao_geral_time_par != 11):
		time_retorno = 1
	if (pontuacao_geral_time_impar != 11) and (pontuacao_geral_time_par == 11):
		time_retorno = 2
	return time_retorno
	
	