extends Node

var pre_carta = preload("res://scenes & scripts/Carta.gd")
var lista_cartas = []
export(int,1,3) var tipo_baralho setget set_tipo_baralho

func _ready():
	pass

func add(carta):
	print(carta.poder)
	lista_cartas.append(carta)
	
func iniciar_baralho():
	zerar_baralho()
	preparar_baralho()
	
func set_tipo_baralho(value):
	tipo_baralho = value
	
func preparar_baralho():
	for i in range(8,14): #cria da dama ao 3
		nova_carta(i)
	if tipo_baralho == 1: #baralho sujo, cria 4,5,6,7
		nova_carta(1) #4
		nova_carta(2) #5
		nova_carta(3) #6
		nova_carta(4) #7
	elif tipo_baralho == 2: #baralho completo, cria 4,5,6,7,8,9,10
		nova_carta(1) #4
		nova_carta(2) #5
		nova_carta(3) #6
		nova_carta(4) #7
		nova_carta(5) #8 
		nova_carta(6) #9
		nova_carta(7) #10

func nova_carta(valor):
	add_carta(valor,1)
	add_carta(valor,2)
	add_carta(valor,3)
	add_carta(valor,4)
	
func add_carta(valor,num):
	var carta
	carta = pre_carta.new()
	carta.definir(valor,num,false)
	add(carta)
	
func zerar_baralho():
	get_node("ap1").play_backwards("fazer_vira")
	lista_cartas = []
	
func pegar_carta_aleatoria():
	var procurar = true
	
	while procurar:
		randomize()
		var index = randi() % (lista_cartas.size()-1)
		
		if not lista_cartas[index].get_em_uso():
			lista_cartas[index].set_em_uso(true)
			return lista_cartas[index]
			procurar = false
	
func fazer_vira():
	get_node("Carta").set_carta(pegar_carta_aleatoria())
	get_node("Carta").set_cima(true)
	global.vira = get_node("Carta").get_valor()
	get_node("ap1").play("fazer_vira")
	
	
	