extends Node2D

#carta = valor
#4=1
#5=2
#6=3
#7=4
#8=5
#9=6
#10=7
#Q=8
#J=9
#K=10
#A=11
#2=12
#3=13

#Naipe - vai receber de 1 a 4, sendo:
#Ouro=1
#Espada=2
#Copas=3
#Paus=4

export(int,0,13) var valor = 0 setget set_valor, get_valor
export(int,0,4) var naipe = 0 setget set_naipe, get_naipe
export(bool) var cima = true setget set_cima, get_cima
var manilha = false setget set_manilha, get_manilha 
var poder
var sprite_name = "" setget set_sprite_name
var em_uso = false setget set_em_uso, get_em_uso #marca se a carta está na mão de alguém ou na vira

func _ready():
	set_process(true)
	set_cima(cima)
	global.connect("vira_feita",self,"verificar_manilha")

func _process(delta):
	pass

func calcular_poder():
	if manilha:
		poder = (valor*100)+naipe
	else:
		if global.considerar_naipe:
			poder = nvalor()
		else:
			poder = valor

func set_valor(value):
	valor = value
	calcular_poder()
	atribuir_sprite_name()
	
func set_naipe(value):
	naipe = value
	calcular_poder()
	atribuir_sprite_name()
	
func atribuir_sprite_name():
	if (valor > 0) and (naipe > 0):
		set_sprite_name("res://sprites/Cards/" + str(valor) + "." + str(naipe) + ".png")
		

func set_sprite_name(value):
	sprite_name = value
	var spr = load(sprite_name)
	if find_node("Sprite"):
		get_node("Sprite").set_texture(spr)
	

func set_cima(value):
	cima = value
	if find_node("Back"):
		get_node("Back").set_visible(!cima)

func definir(val,np,cm):
	set_valor(val)
	set_naipe(np)
	set_cima(cm)
	
func set_manilha(value):
	manilha = value
	calcular_poder()
	
func get_naipe():
	return naipe
	
func get_valor():
	return valor
	
func get_cima():
	return cima
	
func get_manilha():
	return manilha
	
func set_em_uso(value):
	em_uso = value
	
func get_em_uso():
	return em_uso
	
func set_carta(carta):
	get_node(".").set_valor(carta.get_valor())
	get_node(".").set_naipe(carta.get_naipe())
	get_node(".").set_cima(carta.get_cima())
	get_node(".").set_manilha(carta.get_manilha())
	
func verificar_manilha():
	if global.vira == 0:
		return
#	CASO FOR BARALHO SUJO OU LIMPO, DEVE TRATAR A VIRA:
#		SE FOR BARALHO SUJO E A VIRA FOR 7, A MANILHA É Q
#		SE FOR BARALHO LIMPO E A VIRA FOR 3, A MANILHA É Q
	if valor == 8: # 8 = carta Q (dama)
		if global.tipo_baralho == 0: #baralho limpo
			if global.vira == 13: # 13 = carta 3
				set_manilha(true)
		if global.tipo_baralho == 1: #baralho sujo
			if global.vira == 4:  #  4 = carta 7
				set_manilha(true)
		if global.tipo_baralho == 2: #baralho completo
			set_manilha((valor) == (global.vira+1))
	elif valor == 1: # 1 = Carta 4
		if global.vira == 13: # carta 3
			set_manilha(true)
	else:
		set_manilha((valor) == (global.vira+1))
	
func nvalor():
	return valor+naipe









