extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var result = false

signal confirmou_11

func init(_1,_3):
	$Panel/P1_1.set_carta(_1.C1)
	$Panel/P1_2.set_carta(_1.C2)
	$Panel/P1_3.set_carta(_1.C3)
	$Panel/P3_1.set_carta(_3.C1)
	$Panel/P3_2.set_carta(_3.C2)
	$Panel/P3_3.set_carta(_3.C3)
	$Panel/nome_aliado.text = $Panel/nome_aliado.text.replace("%NAME%",_3.player_name)
	pass
	
	
	
func resultado_confirmacao():
	return result

func _on_btnJogar_pressed():
	result = true
	emit_signal("confirmou_11")
	pass # replace with function body


func _on_btnFugir_pressed():
	result = false
	emit_signal("confirmou_11")
	pass # replace with function body
