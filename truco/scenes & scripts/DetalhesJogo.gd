extends CanvasLayer

onready var btl = get_node("Control/btn_limpo")
onready var bts = get_node("Control/btn_sujo")

func _ready():
	pass


func _on_TextureButton_pressed():
	if btl.pressed:
		global.tipo_baralho = 0
	else:
		global.tipo_baralho = 1
	global.nome_player = get_node("Control/TextEdit").text
	if (global.tipo_baralho >= 0) and (global.nome_player != "") :
		print(global.tipo_baralho)
		get_tree().change_scene("res://scenes & scripts/Main.tscn")
	pass


func _on_btn_limpo_pressed():
	btl.pressed = true
	bts.pressed = false
	pass # replace with function body


func _on_btn_sujo_pressed():
	btl.pressed = false
	bts.pressed = true
	pass # replace with function body
