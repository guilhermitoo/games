extends Node

func _ready():
	set_process(true)
	get_node("Mesa").iniciar_jogo()
	pass

func _process(delta):
	
	pass

func _on_Pause_pressed():
	Pause()
	pass # replace with function body

func Pause():
	if get_tree().paused:
		get_tree().paused = false
		$bg_pause.visible = false
		$Pause.show()
	else:
		get_tree().paused = true
		$bg_pause.visible = true
		$Pause.hide()

func _on_btnSair_pressed():
	get_tree().change_scene("res://scenes & scripts/MenuInicial.tscn")
	Pause()
	pass # replace with function body


func _on_Pause2_pressed():
	Pause()
	pass # replace with function body
