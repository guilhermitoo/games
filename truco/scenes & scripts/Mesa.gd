extends Node2D

const sequencia_base = ["P1","P2","P3","P4"]
var sequencia_jogadas = ["P1","P2","P3","P4"]
var carta_vencendo_rodada
var time_vencendo_rodada
onready var pre_conf_11 = load("res://scenes & scripts/Confirmacao_Onze.tscn")

onready var c1 = get_node("P1/Carta")
onready var c2 = get_node("P2/Carta")
onready var c3 = get_node("P3/Carta")
onready var c4 = get_node("P4/Carta")
onready var baralho = get_node("Baralho")

onready var sequencia_vitoriosa = [c1,c2,c3,c4]

signal deu_carta
signal deu_todas_cartas

func _ready():
	set_process(true)
	baralho.set_tipo_baralho(global.tipo_baralho)
	$P1.set_player_name(global.nome_player)
	$P1.set_ia_player(false)
	$P1.set_mesa(get_node("."))
	$P2.set_ia_player(true)
	$P2.set_mesa(get_node("."))
	$P3.set_ia_player(true)
	$P3.set_mesa(get_node("."))
	$P4.set_ia_player(true)
	$P4.set_mesa(get_node("."))
	
	global.connect("fim_do_turno",self,"iniciar_turno")
	global.connect("fim_da_rodada",self,"finalizar_rodada")
	global.connect("fim_da_partida",self,"iniciar_partida")
	global.connect("fim_do_jogo",self,"finalizar_jogo")
	pass
	
func jogar_carta(carta,nome_player_node,cima):
	var carta_tela = get_node(nome_player_node+"/Carta")
	var player = get_node(nome_player_node)
	carta_tela.set_carta(carta)
	carta_tela.cima = cima
	get_node("ap1").play(nome_player_node)
	carta_tela.verificar_manilha()
	
	if carta_vencendo_rodada == null:
		carta_vencendo_rodada = carta_tela
		time_vencendo_rodada = player.time
	else:
		if carta_vencendo_rodada.poder < carta_tela.poder:
			carta_vencendo_rodada = carta_tela
			time_vencendo_rodada = player.time
	
	calcular_sequencia_vitoriosa()
	
	print("carta_vencendo_rodada:" + str(carta_vencendo_rodada.poder))
	print("time_vencendo_rodada:" + str(time_vencendo_rodada))
	
func iniciar_jogo():
	print("iniciar_jogo()")
	global.iniciarVariaveis()
	iniciar_partida()
	
func iniciar_partida():
	get_node("P1").limpar_mao()
	get_node("P2").limpar_mao()
	get_node("P3").limpar_mao()
	get_node("P4").limpar_mao()
	global.time_trucou = 0
	global.vencedor_primeira_rodada = 0
	global.numero_partida += 1
	global.numero_rodada = 0
	global.set_valor_partida(1)
	redefinir_sequencia('',true)
	global.set_parcial_timpar(0)
	global.set_parcial_tpar(0)
	ocultar_cartas_mesa()
	baralho.iniciar_baralho()
	dar_cartas()
	yield(self,"deu_todas_cartas")
	baralho.fazer_vira()
	if global.timeDeOnze() == $P1.time:
		var conf11 = pre_conf_11.instance()
		conf11.init($P1,$P3)
		get_parent().add_child(conf11)
		yield(conf11,"confirmou_11")
		if ! conf11.resultado_confirmacao():
			conf11.queue_free()
			$P1.fugir()
		else:
			conf11.queue_free()
			iniciar_rodada(true)
	else:
		iniciar_rodada(false)
	
func iniciar_rodada(rodada11):
	ocultar_cartas_mesa()
	carta_vencendo_rodada = null
	time_vencendo_rodada = 0
	global.set_numero_turno(0)
	global.numero_rodada += 1
	if rodada11:
		global.valor_partida_solicitado = 3
		global.valor_partida = 3
		global.time_trucou = $P1.time
	iniciar_turno()
	
func iniciar_turno():
	print("player: "+sequencia_jogadas[global.numero_turno])
	get_node(sequencia_jogadas[global.numero_turno]).iniciar_turno()
	
func maior_carta_mesa(considerar_naipe):
	global.considerar_naipe = considerar_naipe
	
	calcular_sequencia_vitoriosa()
	
	return sequencia_vitoriosa[3]
	
#	c1.verificar_manilha()
#	c2.verificar_manilha()
#	c3.verificar_manilha()
#	c4.verificar_manilha()
#
#	var vitoria = c1
#
#	if vitoria.poder < c2.poder:
#		vitoria = c2
#	if vitoria.poder < c3.poder:
#		vitoria = c3
#	if vitoria.poder < c4.poder:
#		vitoria = c4
#
#	if considerar_naipe:
#		if vitoria.poder == c1.poder:
#			if vitoria.nvalor() < c1.nvalor():
#				vitoria = c1
#		if vitoria.poder == c2.poder:
#			if vitoria.nvalor() < c2.nvalor():
#				vitoria = c2
#		if vitoria.poder == c3.poder:
#			if vitoria.nvalor() < c3.nvalor():
#				vitoria = c3
#		if vitoria.poder == c4.poder:
#			if vitoria.nvalor() < c4.nvalor():
#				vitoria = c4
#	return vitoria
		
	
func finalizar_rodada():
	#calcular as cartas na mesa para ver quem ganhou
	
	#espera tempo
	yield(get_tree().create_timer(5,false), "timeout")
	
	c1.verificar_manilha()
	c2.verificar_manilha()
	c3.verificar_manilha()
	c4.verificar_manilha()
	
	var vitoria = maior_carta_mesa(false)
	var e1 = 0
	var e2 = 0
	
	if (c1.poder == c2.poder) or (c1.poder == c4.poder):
		e1 = c1.poder
	if (c3.poder == c2.poder) or (c3.poder == c4.poder):
		e2 = c3.poder
	
	if ((e1 >= vitoria.poder) or (e2 >= vitoria.poder)) and (global.numero_rodada == 1):
		global.rodada_desempate = true
		print("empate")
		iniciar_rodada(false)
		return # se foi empate na primeira, já inicia rodada de desempate
	elif ((e1 >= vitoria.poder) or (e2 >= vitoria.poder)) and ((global.numero_rodada == 2) or (global.numero_rodada == 3)) and (!global.rodada_desempate):
		print("empate na "+str(global.numero_rodada)+" rodada")
		if global.vencedor_primeira_rodada == 1:
			global.set_parcial_timpar(global.get_parcial_timpar()+1)
		elif global.vencedor_primeira_rodada == 2:
			global.set_parcial_tpar(global.get_parcial_tpar()+1)
		if (global.get_parcial_timpar() == 2) or (global.get_parcial_tpar() == 2):
			global.finalizar_partida()
		return
	elif global.rodada_desempate:
		print("rodada desempate")
		global.rodada_desempate = false
		vitoria = maior_carta_mesa(true)
		global.set_parcial_timpar(global.get_parcial_timpar()+1)
		global.set_parcial_tpar(global.get_parcial_tpar()+1)
		
	var p_vitoria = "P1"
	if vitoria == c2:
		p_vitoria = "P2"
	elif vitoria == c3:
		p_vitoria = "P3"
	elif vitoria == c4:
		p_vitoria = "P4"
	print("vencedor da rodada: " + p_vitoria)
	
	redefinir_sequencia(p_vitoria,false)
		
	if (vitoria == c1) or (vitoria == c3):
		if global.numero_rodada == 1:
			global.vencedor_primeira_rodada = 1
			print("vencedor primeira rodada: time impar")
		global.set_parcial_timpar(global.get_parcial_timpar()+1)
	elif (vitoria == c2) or (vitoria == c4):
		if global.numero_rodada == 1:
			global.vencedor_primeira_rodada = 2
			print("vencedor primeira rodada: time par")
		global.set_parcial_tpar(global.get_parcial_tpar()+1)
	
	if (global.get_parcial_timpar() == 2) or (global.get_parcial_tpar() == 2):
		global.finalizar_partida()
	else:
		iniciar_rodada(false)
	
func redefinir_sequencia(nome,nova_partida):
	var index = 0
	
	if ! nova_partida:
		for i in range(0,4):
			if nome == sequencia_base[i]:
				index = i
	else:
		index = global.numero_partida-1
	
	var ind = 0
	for i in range (0,4):
		ind = index%4
		sequencia_jogadas[i] = sequencia_base[ind]
		index+=1
		
	print(sequencia_jogadas)
	
	
	
func dar_cartas():
	#método para dar as cartas
	dar_carta_jogador(sequencia_jogadas[1])
	yield(self,"deu_carta")
	dar_carta_jogador(sequencia_jogadas[2])
	yield(self,"deu_carta")
	dar_carta_jogador(sequencia_jogadas[3])
	yield(self,"deu_carta")
	dar_carta_jogador(sequencia_jogadas[0])
	yield(self,"deu_carta")
	emit_signal("deu_todas_cartas")
	
func dar_carta_jogador(player):
	print("dando cartas jogador "+player)
	var p = get_node(player)
	var rot = 0
	if player == "P1":
		rot = 90
	elif player == "P2":
		rot = 0
	elif player == "P3":
		rot = -90
	elif player == "P4":
		rot = 180
	get_node("Baralho/Baralho_desk").rotation_degrees = rot
	$ap1.play("dar_cartas") 
	yield($ap1,"animation_finished")
	p.dar_carta(1,baralho.pegar_carta_aleatoria())
	$ap1.play("dar_cartas") 
	yield($ap1,"animation_finished")
	p.dar_carta(2,baralho.pegar_carta_aleatoria())
	$ap1.play("dar_cartas") 
	yield($ap1,"animation_finished")
	p.dar_carta(3,baralho.pegar_carta_aleatoria())
	
	emit_signal("deu_carta")

func _process(delta):
	pass
	
func ocultar_cartas_mesa():
	get_node("P1/Carta").hide()
	get_node("P2/Carta").hide()
	get_node("P3/Carta").hide()
	get_node("P4/Carta").hide()

func get_parceiro(caller,time):
	if time == 1:
		if caller == "P1":
			return $P3
		else:
			return $P1
	elif time == 2:
		if caller == "P2":
			return $P4
		else:
			return $P2

func calcular_sequencia_vitoriosa():
	#calcula sequencia vitoriosa, sendo indice 0 a mais fraca
	# e indice 3 a mais forte	
	var carta1
	var carta2
	
	for j in range(4):
		for i in range(3):
			sequencia_vitoriosa[i].verificar_manilha()
			sequencia_vitoriosa[i+1].verificar_manilha()
			
			carta1 = sequencia_vitoriosa[i]
			carta2 = sequencia_vitoriosa[i+1]
			if ((carta1.poder > carta2.poder) and carta1.cima) or (! carta2.cima):
				sequencia_vitoriosa[i+1] = carta1
				sequencia_vitoriosa[i] = carta2
				
	print("carta vencendo rodada (array): "+str(sequencia_vitoriosa[3].poder))

func finalizar_jogo():
	get_tree().change_scene("res://scenes & scripts/PlacarFinal.tscn")
