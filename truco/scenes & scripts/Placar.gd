extends Control


func _ready():
	set_process(true)
	global.connect("placar_geral_mudou",self,"marcar_pontuacao_geral")
	global.connect("placar_parcial_mudou",self,"marcar_pontuacao_parcial")
	global.connect("turno_mudou",self,"mudar_jogador_atual")
	global.connect("valor_partida_mudou",self,"mudar_valor_partida")
	pass

func _process(delta):
	pass
	
func marcar_pontuacao_geral():
	get_node("fundo/lblGeral_par").text = str(global.pontuacao_geral_time_par)
	get_node("fundo/lblGeral_impar").text = str(global.pontuacao_geral_time_impar)
	pass
	
func marcar_pontuacao_parcial():
	get_node("fundo/lblRodada_par").text = str(global.pontuacao_parcial_time_par)
	get_node("fundo/lblRodada_impar").text = str(global.pontuacao_parcial_time_impar)
	pass
	
func mudar_jogador_atual():
	get_node("fundo_top/lblPlayer").text = global.nome_player_turno_atual
	
func mudar_valor_partida():
	get_node("fundo/lblTentos").text = str(global.valor_partida)
	
	

