extends Node2D



var pre_carta = preload("res://scenes & scripts/Carta.gd")
var player_name = "" setget set_player_name
var mesa setget set_mesa
var meu_turno = false setget set_meu_turno
var ia_player = false setget set_ia_player
var ult_carta
var mostrar_cartas = false
export(int,1,2) var time = 1 setget set_time,get_time
export(bool) var inverter_horizontal = false


onready var pb = get_node("Perfil/pb_turno")
onready var pic = get_node("Perfil/picture")
onready var C1 = get_node("Cards/Carta1")
onready var C2 = get_node("Cards/Carta2")
onready var C3 = get_node("Cards/Carta3")

var chance_vitoria = 0

signal avaliei_truco #signal que informa quando o player acabou de avalir o truco, mas ele não faz a escolha final

func _ready():
	set_process(true)
	global.connect("truco",self,"analisar_truco")
	global.connect("valor_partida_mudou",self,"ajustar_botao_truco")
	global.connect("truco_confirmado",self,"retomar_turno")
	ocultar_botoes()
	ajustar_inverter_horizontal()
	$AnimationPlayer.get_animation("progress_bar").length = global.tempo_duracao_turno
	pb.max_value = global.tempo_duracao_turno
	pb.value = global.tempo_duracao_turno
	$AnimationPlayer.get_animation("progress_bar").add_track(0)
	$AnimationPlayer.get_animation("progress_bar").step = 0.1
	$AnimationPlayer.get_animation("progress_bar").track_set_path(0,"Perfil/pb_turno:value")
	$AnimationPlayer.get_animation("progress_bar").track_insert_key(0,0,global.tempo_duracao_turno)
	$AnimationPlayer.get_animation("progress_bar").track_insert_key(0,global.tempo_duracao_turno,0)
	
func ajustar_inverter_horizontal():
	if pic.visible:
		if inverter_horizontal:
			$Cards.position.x = 170
		else:
			$Cards.position.x = 100

func set_ia_player(value):
	ia_player = value
	if ia_player:
		define_ia_player()

func define_ia_player():
	randomize()
	var r = rand_range(0,global.pictures.size()-1)
	if ! global.pictures[r]["in_use"]:
		set_player_name(global.pictures[r]["name"])
		set_player_picture(global.pictures[r]["path"])
		global.pictures[r]["in_use"] = true
	else:
		define_ia_player()

func set_player_name(value):
	player_name = value
	get_node("Perfil/clrBackgroundName/lbl_nome").text = player_name
	
func set_player_picture(path):
	pic.texture = load(path)

func set_mesa(value):
	#define qual mesa o jogador está vinculado
	mesa = value

func jogar_carta(nome,cima):
	if meu_turno and (get_node("Cards/"+nome).visible) and (mesa.sequencia_jogadas[global.numero_turno] == name):
		mesa.jogar_carta(get_node("Cards/"+nome),get_node(".").name,cima)
		ult_carta = get_node("Cards/"+nome)
		get_node("Cards/"+nome).hide()
		set_meu_turno(false)
		global.finalizar_turno()

func iniciar_turno():
	global.set_player_turno_atual(player_name)
	
	if !ia_player:
		global.exibirTexto("Sua vez de jogar!")
	
	set_meu_turno(true)
	
	realizar_jogada()
	
func dar_carta(numero,carta):
	match numero:
		1: 
			get_node("Cards/Carta1").set_carta(carta)
			get_node("Cards/Carta1").show()
		2: 
			get_node("Cards/Carta2").set_carta(carta)
			get_node("Cards/Carta2").show()
		3: 
			get_node("Cards/Carta3").set_carta(carta)
			get_node("Cards/Carta3").show()
		
	get_node("Cards/Carta1").set_cima((not ia_player) or global.modo_teste or mostrar_cartas)
	get_node("Cards/Carta2").set_cima((not ia_player) or global.modo_teste or mostrar_cartas)
	get_node("Cards/Carta3").set_cima((not ia_player) or global.modo_teste or mostrar_cartas)

func _on_tsb_carta1_pressed():
	# código ao tocar na carta 1. SE for seu turno, joga a carta na mesa
	if ! ia_player:
		jogar_carta('Carta1',true)
	pass # replace with function body

func _on_tsb_carta2_pressed():
	if ! ia_player:
		jogar_carta("Carta2",true)
	pass # replace with function body

func _on_tsb_carta3_pressed():
	if ! ia_player:
		jogar_carta("Carta3",true)
	pass # replace with function body
	
func realizar_jogada():
	if ia_player:
		#espera tempo aleatório entre 1 e 7 segundos
		randomize()
		var tempo = rand_range(2,9) #número aleatório de 2 a 9
		print('tempo espera: '+str(tempo)+' segundos')
		yield(get_tree().create_timer(tempo,false), "timeout")
		
		var index = 0
		var cima = true
		calcular_chance_vitoria()
		var CV = chance_vitoria
		var deve_trucar = false
		if (CV >= 95) and (global.numero_rodada == 1):
			deve_trucar = true
		elif (CV >= 100) and (global.numero_rodada == 2):
			deve_trucar = true
		elif (CV >= 97):
			deve_trucar = true
		else:
			if blefar() and global.valor_partida_solicitado == 3:
				deve_trucar = true
			else:
				deve_trucar = false
				
		# verifica se deve jogar quieto, pois, as vezes o jogador joga esperando ser trucado
		randomize()
		var jogar_quieto = (rand_range(1,100) < global.chance_jogar_quieto)
				
		if deve_trucar and !jogar_quieto:
			if trucar():
				return
		
		if ! (mesa.carta_vencendo_rodada == null):
			mesa.carta_vencendo_rodada.verificar_manilha()
			if (mesa.time_vencendo_rodada != time):
				index = carta_mais_forte()
				
				# verifica se mesmo pegando a melhor carta, ela ainda é mais fraca que a carta mais forte da mesa
				if get_node("Cards/Carta"+str(index)).poder < mesa.carta_vencendo_rodada.poder:
					index = carta_mais_fraca()
					# verifica se a segunda carta mais forte da conta de matar a rodada, se sim, usa ela e guarda
					# a carta mais forte da mão
				elif get_node("Cards/Carta"+str(carta_mais_fraca())).poder > mesa.carta_vencendo_rodada.poder:
					index = carta_mais_fraca()
				elif get_node("Cards/Carta"+str(carta_media())).poder > mesa.carta_vencendo_rodada.poder:
					index = carta_media()
			else:
				index = carta_mais_fraca()
				
				randomize()
				var jogar_escondido = (rand_range(1,100) < global.chance_jogar_escondido) #número aleatório de 1 a 100
				
				if jogar_escondido and (global.numero_rodada != 1):
					cima = false
		else:
			index = carta_mais_forte()
		
		jogar_carta("Carta"+str(index),cima)
	
func get_time():
	return time
	
func set_time(value):
	time = value
	
func carta_mais_fraca():
	#retorna o indice de 1 a 3 da carta mais fraca da mão
	var ind = 0
	var carta
	var poder = 100000
	for i in range(1,4):
		carta = get_node("Cards/Carta"+str(i))
		carta.verificar_manilha()
		if carta.visible:
			if poder > carta.poder:
				poder = carta.poder
				ind = i
	return ind
	pass
	
func carta_media(): #entre a mais forte e a mais fraca
	var cmf = carta_mais_forte()
	var ind = cmf
	var carta
	var poder = 0
	for i in range(1,4):
		carta = get_node("Cards/Carta"+str(i))
		carta.verificar_manilha()
		if carta.visible and (i != cmf):
			if poder < carta.poder:
				poder = carta.poder
				ind = i
	return ind
	
func carta_mais_forte():
	#retorna o indice de 1 a 3 da carta mais forte da mão
	var ind = 0
	var carta
	var poder = 0
	for i in range(1,4):
		carta = get_node("Cards/Carta"+str(i))
		carta.verificar_manilha()
		if carta.visible:
			if poder < carta.poder:
				poder = carta.poder
				ind = i
				
	return ind
	pass
	
func carta_aleatoria():
	#retorna o indice de 1 a 3 de uma carta aleatoria
	randomize()
	var index = (randi() % 3) + 1 #número aleatório de 1 a 3	
	
	# garante que não pegou uma carta invisível
	while ( ! get_node("Cards/Carta"+str(index)).visible ):
		randomize()
		index = (randi() % 3) + 1 #número aleatório de 1 a 3
		
	return index

func trucar():
	if (global.valor_partida < 12) and (global.time_trucou != time) and (global.timeDeOnze() != time):
		set_meu_turno(false)
		global.exibirTexto(player_name+" pediu "+global.textoProxTruco(global.valor_partida))
		global.truco(time)
		return true
	return false
	
func aceitar_truco():
	print("jogador "+name+" aceitou!")
	global.exibirTexto(player_name+" aceitou "+global.textoProxTruco(global.valor_partida))
	set_meu_turno(false)
	global.confirmar_truco()
	pass
	
func aumentar_truco():
	set_meu_turno(false)
	print("jogador "+name+" retrucou!")
	if global.valor_partida_solicitado < 12:
		global.exibirTexto(player_name+" pediu "+global.textoProxTruco(global.valor_partida_solicitado))
		global.aumentar_truco(time)
	else:
		aceitar_truco()
	
func recusar_truco():
	fugir()
	pass
	
func analisar_truco():
	if global.time_trucou != time:
		if ! ia_player:
			if (mesa.sequencia_jogadas[global.numero_prox_turno] == name) or (mesa.sequencia_jogadas[global.numero_turno] == name):
				exibir_confirmacao_truco(true)
		else:
			# verifica se ele é o jogador que foi trucado ou o que está recebendo retruco
			if (mesa.sequencia_jogadas[global.numero_prox_turno] == name) or (mesa.sequencia_jogadas[global.numero_turno] == name):
				#se sim, então pergunta para seu parceiro qual a situacao da mao dele
				
				var parceiro = mesa.get_parceiro(name,time)
				parceiro.avaliar_truco()
				if ! parceiro.ia_player:
					yield(parceiro,"avaliei_truco")
				print("parceiro avaliou")
				set_meu_turno(true)
				
				randomize()
				var tempo = rand_range(2,7) #número aleatório de 2 a 7
				print('tempo espera: '+str(tempo)+' segundos')
				yield(get_tree().create_timer(tempo,false), "timeout")
				
				#PCV chance de vitória do parceiro
				var PCV = parceiro.chance_vitoria
				calcular_chance_vitoria()
				var CV = chance_vitoria
				
				# ajusta a chance de vitória de acordo com as cartas do aliado
				if PCV <= 25:
					CV -= 20
				elif PCV <= 50:
					CV -= 5
				elif PCV <= 75:
					CV = CV
				else:
					CV += 25
				print("chance vitoria ajustada: "+str(CV))
				
				if CV >= 100:
					aumentar_truco()
				elif CV >= 90:
					if global.valor_partida < 6:
						aumentar_truco()
					else:
						aceitar_truco()
				elif CV >= 50:
					aceitar_truco()
				else:
					if blefar() and global.valor_partida_solicitado == 3:
						aceitar_truco()
					else:
						recusar_truco()
			
			
	pass
	
func avaliar_truco():
	if ! ia_player:
		exibir_confirmacao_truco(false)
	else:
		calcular_chance_vitoria()
	emit_signal("avaliei_truco")
	
	
func exibir_confirmacao_truco(analisar):
	print("exibir confirmação do truco")
	exibir_botoes(analisar)
	
	set_meu_turno(true)
	pass
	
func _on_btn_Truco_pressed():
	trucar()
	pass # replace with function body

func set_meu_turno(value):
	meu_turno = value
	
	if meu_turno:
		ajustar_botao_truco()
		get_node("AnimationPlayer").play("progress_bar")
		if has_node("ap_iniciar_turno"):
			get_node("ap_iniciar_turno").play("exibir_botoes")
	else:
		get_node("AnimationPlayer").stop(false)
		#pb.visible = false
		pb.value = pb.max_value
		if has_node("ap_iniciar_turno"):
			get_node("ap_iniciar_turno").play_backwards("exibir_botoes")
	
func calcular_chance_vitoria():
	var CV = 100
	
	var icmf = carta_mais_forte()
	var icm  = carta_media()
	var mais_forte
	var carta_meio
	
	if icmf != 0:
		mais_forte = get_node("Cards/Carta"+str(icmf))
		carta_meio = get_node("Cards/Carta"+str(icm))
	else: # se ja nao tem cartas na mao, analisa usando a sua que está na mesa
		mais_forte = ult_carta
	
	if global.numero_rodada == 2:
		if global.vencedor_primeira_rodada != time:
			CV -= 23
	
	if mais_forte.poder < 8:    # Q
		CV -= 53
	elif mais_forte.poder <= 11:# A
		CV -= 40
	elif mais_forte.poder <= 12:# 2
		CV -= 28
	elif mais_forte.poder <= 13:# 3
		CV -= 16 
		
	if global.numero_rodada != 3:
		if carta_meio.poder < 8:    # Q
			CV -= 38
		elif carta_meio.poder <= 11:# A
			CV -= 26
		elif carta_meio.poder <= 12:# 2
			CV -= 12
			
	if global.valor_partida == 3:
		CV -= 10
	elif global.valor_partida == 6:
		CV -= 20
	elif global.valor_partida == 9:
		CV -= 30
		
	print(str(CV))
	chance_vitoria = CV

func blefar():
	randomize()
	var chance = rand_range(1,100)
	
	return (chance < global.chance_blefe)
	
	
func _on_AnimationPlayer_animation_started(anim_name):
	#if anim_name == "progress_bar":
		#pb.visible = true
	pass # replace with function body


func _on_AnimationPlayer_animation_finished(anim_name):
	if (anim_name == "progress_bar"): 
		if (! global.analise_truco):
			#pb.visible = false
			finalizar_turno_forcado()
		else:
			finalizar_analise_forcado()
	pass # replace with function body

func finalizar_turno_forcado():
	jogar_carta("Carta"+str(carta_aleatoria()),true)
	pass

func finalizar_analise_forcado():
	recusar_truco()

func _on_btn_Fugir_pressed():
	fugir()
	pass # replace with function body

func fugir():
	set_meu_turno(false)
	global.exibirTexto(player_name+" fugiu...")
	global.fugir(time)
	
func limpar_mao():
	C1.hide()
	C2.hide()
	C3.hide()
	
func ajustar_botao_truco():
	ajustar_onze()
	if has_node("botoes_turno"):
		$botoes_turno/btn_Truco.disabled = (global.time_trucou == time) or (global.valor_partida == 12)
		$botoes_turno/btn_Truco.visible = true
		$botoes_turno/btn_Fugir.visible = true
		
		var bt = global.textoProxTruco(global.valor_partida)
		
		if global.valor_partida == 12:
			$botoes_turno/btn_Truco.visible = false
		$botoes_turno/btn_Truco.text = bt
		
		$botoes_turno/btn_AumentarTruco.disabled = false
		bt = global.textoProxTruco(global.valor_partida_solicitado)
		
		if global.valor_partida_solicitado == 12:
			$botoes_turno/btn_AumentarTruco.disabled = true
			
		$botoes_turno/btn_AumentarTruco.text = bt
		
	
func exibir_botoes(analisar):
	
	if has_node("botoes_turno"):
		$botoes_turno/btn_Truco.visible = false
		if ! analisar:
			$botoes_turno/btn_DeuBom.visible = true
			$botoes_turno/btn_DeuRuim.visible = true
			$botoes_turno/btn_MaisMenos.visible = true
			$botoes_turno/btn_GritaNele.visible = true
		else:
			$botoes_turno/btn_AceitarTruco.visible = true
			$botoes_turno/btn_AumentarTruco.visible = true
	pass

func ocultar_botoes():
	yield(get_tree().create_timer(1.5,false), "timeout")
	
	if has_node("botoes_turno"):
		$botoes_turno/btn_AceitarTruco.visible = false
		$botoes_turno/btn_DeuBom.visible = false
		$botoes_turno/btn_AumentarTruco.visible = false
		$botoes_turno/btn_DeuRuim.visible = false
		$botoes_turno/btn_MaisMenos.visible = false
		$botoes_turno/btn_GritaNele.visible = false
		$botoes_turno/btn_Truco.visible = false
		$botoes_turno/btn_Fugir.visible = false

func retomar_turno():
	#função que deve recomeçar o turno de quem pediu o truco inicialmente
	print("retomar turno: "+global.nome_player_turno_atual)
	if player_name == global.nome_player_turno_atual:
		set_meu_turno(true)
		realizar_jogada()

func _on_btn_AceitarTruco_pressed():
	aceitar_truco()
	ocultar_botoes()
	pass # replace with function body


func _on_btn_MaisMenos_pressed():
	set_meu_turno(false)
	chance_vitoria = 50
	ocultar_botoes()
	emit_signal("avaliei_truco")
	pass # replace with function body


func _on_btn_AumentarTruco_pressed():
	aumentar_truco()
	ocultar_botoes()
	pass # replace with function body


func _on_btn_DeuBom_pressed():
	set_meu_turno(false)
	chance_vitoria = 75
	ocultar_botoes()
	emit_signal("avaliei_truco")
	pass # replace with function body


func _on_btn_DeuRuim_pressed():
	set_meu_turno(false)
	chance_vitoria = 25
	ocultar_botoes()
	emit_signal("avaliei_truco")
	pass # replace with function body


func _on_btn_GritaNele_pressed():
	set_meu_turno(false)
	chance_vitoria = 100
	ocultar_botoes()
	emit_signal("avaliei_truco")
	pass # replace with function body


func ajustar_onze():
	var parceiro = mesa.get_parceiro(name,time)
	parceiro.mostrar_cartas = false
	if not ia_player:
		if global.timeDeOnze() == time:
			parceiro.mostrar_cartas = true
			
			




