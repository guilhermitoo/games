extends Node

const GROUP_ENEMY = "ENEMY"
const GROUP_MAP_OBJECT = "MAP_OBJECT"
const GROUP_PLAYER = "PLAYER"
const GROUP_MUNITION = "MUNITION"

enum TimeFormat {
	FORMAT_HOURS   = 1 << 0,
	FORMAT_MINUTES = 1 << 1,
	FORMAT_SECONDS = 1 << 2}

#const resolution = Vector2(1154,648)
var preblood = preload("res://scenes & scripts/blood1.tscn")

const guns = [preload("res://scenes & scripts/guns/revolver.tscn"),
			  preload("res://scenes & scripts/guns/pistola.tscn"),
			  preload("res://scenes & scripts/guns/pistola_2.tscn"),
			  preload("res://scenes & scripts/guns/uzi.tscn"),
			  preload("res://scenes & scripts/guns/uzi_2.tscn"),
			  preload("res://scenes & scripts/guns/mp5.tscn"),
			  preload("res://scenes & scripts/guns/mp5_2.tscn"),
			  preload("res://scenes & scripts/guns/ak47.tscn"),
			  preload("res://scenes & scripts/guns/m16.tscn"),
			  preload("res://scenes & scripts/guns/m16_2.tscn"),
			  preload("res://scenes & scripts/guns/escopeta.tscn"),
			  preload("res://scenes & scripts/guns/giratoria.tscn")]
			
const maps = [preload("res://scenes & scripts/map_0.tscn"),
			  preload("res://scenes & scripts/map_1.tscn")]

var control_type = 2

var player_default_hp = 30

var show_virtual_buttons : bool = true

var PLAYER_NAME = ""
var player_hp = 0 setget set_player_hp
var player_max_mun = 0 setget set_player_max_mun
var player_actual_mun = 0 setget set_player_actual_mun
var gun_interval = 0 setget set_gun_interval
var reload_interval = 0 setget set_reload_interval

var map_difficulty = 0
var last_level = 13
var money_per_level : int = 2500

# INFORMAÇÕES PARA SEREM SALVAS E CARREGADAS AO ABRIR O JOGO
# INICIO
var gun_equipped = 0
var map_selected = 0
var level = 1 setget set_level
var completed_objectives = []
var guns_acquired = [0] setget set_guns_acquired
var wallet = 0 setget set_wallet # carteira de dinheiro

var enemies_killed = 0 setget set_enemies_killed #somatória da partida
var enemies_killed_general = 0 # somatória geral
var enemies_killed_best = 0 # maior quantidade de inimigos mortos em uma partida

var boss_killed = 0 setget set_boss_killed
var boss_killed_general = 0
var boss_killed_best = 0

var time_spent = 0 setget set_time_spent# somatória tempo
var time_spent_general = 0 # somatória tempo geral
var time_spent_best = 0 # melhor tempo

var combo_best = 0 # melhor combo geral
var combo = 0 setget set_combo # combo dentro da partida
var maxcombo_game = 0 # melhor combo dentro da partida

var damage_deal = 0
var damage_deal_general = 0
var damage_deal_best = 0

var damage_taken = 0
var damage_taken_general = 0
var damage_taken_best = 0
# MENU OPÇÕES
var show_fps : bool = true
# FIM

var enemy_alive = 0

signal hp_changed
signal mun_changed
signal wallet_changed
signal player_died

signal enemy_killed
signal combo
signal minute_changed
signal boss_killed

signal level_changed
signal gun_interval_changed
signal reload_interval_changed

func _ready():
	match OS.get_name():
		"Android","iOS": 
			control_type = 1
		_: 
			control_type = 2
		
	self.connect("enemy_killed",self,"increment_combo")
	
	pass

func get_inserted_walk_direction() -> Vector2:
	var x = 0
	var y = 0
	if Input.is_action_pressed("ui_up"):
		y = -2
	if Input.is_action_pressed("ui_right"):
		x = 2
	if Input.is_action_pressed("ui_left"):
		x = -2
	if Input.is_action_pressed("ui_down"):
		y = 2

	var dir = Vector2(x,y)
	return dir
	
func is_shooting():
	return Input.is_action_pressed("ui_fire")
	
func get_main():
	return get_tree().get_root().get_node("game")
	pass
	
func set_player_hp(value):
	player_hp = value
	emit_signal("hp_changed")
	if player_hp <= 0:
		emit_signal("player_died")
	
func set_player_max_mun(value):
	player_max_mun = value
	emit_signal("mun_changed")
	
func set_player_actual_mun(value):
	player_actual_mun = value
	emit_signal("mun_changed")
	
func format_float( val ):
	var s = str(val)
	var a = ""
	var c = 0
	for i in range(s.length()-1,-1,-1):
		if c == 3:
			a = "."+a
			c = 0
		a = s[i]+a
		c += 1
	return str(a)
	
func set_wallet(value):
	wallet = value
	emit_signal("wallet_changed")
	
func set_enemies_killed(value):
	enemies_killed = value
	emit_signal("enemy_killed")
	
func format_time(time, format = TimeFormat.FORMAT_HOURS | TimeFormat.FORMAT_MINUTES | TimeFormat.FORMAT_SECONDS, digit_format = "%02d"):
	var digits = []
	
	var seconds:float = fmod(time , 60.0)
	var minutes:int   =  int(time / 60.0) % 60
	var hours:  int   =  int(time / 3600.0)
	var formatted : String = ""
	
	if format & TimeFormat.FORMAT_HOURS:
		formatted = formatted + (digit_format % hours)
	if format & TimeFormat.FORMAT_MINUTES:
		if formatted != "":
			formatted = formatted + ":"
		formatted = formatted + (digit_format % minutes)
	if format & TimeFormat.FORMAT_SECONDS:
		if formatted != "":
			formatted = formatted + ":"
		formatted = formatted + (digit_format % seconds)
	return formatted
	
func finish_score():
	enemies_killed_general += enemies_killed
	if enemies_killed > enemies_killed_best:
		enemies_killed_best = enemies_killed
	enemies_killed = 0
	
	time_spent_general += time_spent
	if time_spent > time_spent_best:
		time_spent_best = time_spent
	time_spent = 0
	
	if maxcombo_game > combo_best:
		combo_best = maxcombo_game
	maxcombo_game = 0
	combo = 0
	
	damage_deal_general += damage_deal
	if damage_deal > damage_deal_best:
		damage_deal_best = damage_deal
	damage_deal = 0
	
	damage_taken_general += damage_taken
	if damage_taken > damage_taken_best:
		damage_taken_best = damage_taken
	damage_taken = 0
	
	boss_killed_general += boss_killed
	if boss_killed > boss_killed_best:
		boss_killed_best = boss_killed
	boss_killed = 0
	
	enemy_alive = 0
	
func set_guns_acquired(value):
	# garante que o array se manterá com valores inteiros
	guns_acquired.clear()
	for i in value:
		guns_acquired.append(int(i))
	
func set_combo(value):
	combo = value
	if combo > maxcombo_game:
		maxcombo_game = combo
	emit_signal("combo")
	
func set_time_spent(value):
	time_spent = value
	if (int(time_spent) % 60) == 0:
		emit_signal("minute_changed")
	
func set_level(value):
	level = value
	emit_signal("level_changed")
	
func set_boss_killed(value):
	boss_killed = value
	emit_signal("boss_killed")
	
func increment_combo():
	set_combo(combo + 1)
	
func set_gun_interval(value):
	gun_interval = value
	emit_signal("gun_interval_changed")
	
func set_reload_interval(value):
	reload_interval = value
	emit_signal("reload_interval_changed")
	
func get_proportion():
	var w = get_viewport().get_visible_rect().size.x
	var h = get_viewport().get_visible_rect().size.y
	var prop = w/h
	
	return prop
	
func _on_sound_finished(s):
	#finaliza o som
	s.queue_free()
	pass # replace with function body
	
func globalsound(path):
	var s = AudioStreamPlayer.new()
	s.connect("finished",self,"_on_sound_finished",[s])
	s.stream = load(path)
	get_tree().get_root().add_child(s)
	s.play()
	
func clicksound():
	globalsound("res://sounds/click1.ogg")
	
	
func reloadsound():
	globalsound("res://sounds/gun-reload.wav")
	pass

