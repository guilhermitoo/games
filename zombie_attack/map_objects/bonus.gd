extends Node2D

var pre_box = preload("res://map_objects/box.tscn")

func _ready():
	$ap.play("create")
	pass

func _on_ap_animation_finished(anim_name):
	var b = pre_box.instance()
	b.position = position
	get_node("../").add_child(b)
	self.queue_free()
	pass # replace with function body
