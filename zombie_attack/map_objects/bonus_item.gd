extends Area2D

export var method_effect = ""

func _ready():
	if $anim.has_animation("idle"):
		$anim.play("idle")
	pass

func destroy():
	queue_free()

func _on_bonus_item_body_entered(body):
	if body.has_method(method_effect):
		body.call(method_effect)
		destroy()
	pass # replace with function body
