extends "res://map_objects/map_object.gd"

var bonuses = ["res://map_objects/heal.tscn"]

func _ready():
	pass

func _destroy():
	if has_node("anim"):
		if $anim.has_animation("destroy"):
			$anim.play("destroy")

func _on_anim_animation_finished(anim_name):
	if anim_name == "destroy":
		create_bonus_item()
	pass # replace with function body
	
func create_bonus_item():
	randomize()
	var i = randi()%bonuses.size()
	var a = load(bonuses[i])
	var b = a.instance()
	b.position = self.position
	get_node("../").add_child(b)
	._destroy()