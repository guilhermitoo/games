extends Area2D

var mdl = 70

func _process(delta):
	pass

func _on_green_tree_body_entered(body):
	if body.is_in_group(global.GROUP_PLAYER):
		$tree.modulate.a8 = mdl
	pass # replace with function body


func _on_green_tree_body_exited(body):
	if body.is_in_group(global.GROUP_PLAYER):
		$tree.modulate.a8 = 255
	pass # replace with function body

func _on_StaticBody2D_map_object_destroyed():
	$CollisionShape2D.disabled = true
	$tree.hide()
	self.z_index = 0
	pass # replace with function body

func _destroy():
	queue_free()



