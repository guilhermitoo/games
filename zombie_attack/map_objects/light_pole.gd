extends StaticBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var interval = 1
var i = 0
export var blink = true

# Called when the node enters the scene tree for the first time.
func _ready():
	if !blink:
		return
	randomize()
	interval = rand_range(0.5,2)
	set_process(true)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	i += delta
	if i > interval:
		randomize()
		$Light2D.enabled = (rand_range(0,2) > 1)
		i = 0
