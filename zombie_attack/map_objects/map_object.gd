extends StaticBody2D

export var hitpoints = 100
export(Texture) var destroyed_texture = null

signal map_object_destroyed

func _ready():
	add_to_group(global.GROUP_MAP_OBJECT)
	pass
	
func take_damage(damage):
	hitpoints -= damage
	
	if has_node("anim"):
		if $anim.has_animation("take_damage"):
			$anim.play("take_damage")
	if hitpoints <= 0:
		_destroy()
	pass
	
func _destroy():
	emit_signal("map_object_destroyed")
	if destroyed_texture != null:
		var a = Sprite.new()
		a.texture = destroyed_texture
		a.position = self.position
		a.z_index = 0
		get_node("../").add_child(a)
	queue_free()
	pass

