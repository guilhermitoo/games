extends Node

# EK = Enemy Killed
# TS = Time Spent (em minutos)
# CB = Combo
# BS = Boss

const sek = "ek" # enemies killed
const sts = "ts" # time spent
const sbs = "bs" # boss killed
const scb = "cb" # best combo
const map = "map" # map

# constantes dos objetivos
const ek_msg = "Abater %d inimigos"
const ts_msg = "Sobreviver %d minutos"
const cb_msg = "Realizar um combo de %d abates"
const bs_msg = "Eliminar %d chefes" 
const map_msg = "Mapa: %s"

var obj =  {
			"1" : { sts:  1,  sek:  40 },
			"2" : { sts:  2,  sek:  70,  scb: 25 },    
			"3" : { sts:  3,  sek:  90,  scb: 50 },
			"4" : { sts:  4,  sek: 150,  scb: 70 },
			"5" : { sts:  5,  sek: 220,  scb: 100, map: 1 },
			"6" : { sts:  6,  sek: 400,  scb: 150, map: 1 },
			"7" : { sts:  7,  sek: 600,  scb: 200, map: 1 },
			"8" : { sts:  8,  sek: 800,  scb: 300, map: 1 },
			"9" : { sts:  9,  sek: 1000, scb: 400, map: 2 },
			"10": { sts: 10,  sek: 1400, scb: 500, map: 2 },
			"11": { sts: 11,  sek: 1800, scb: 600, map: 2 },
			"12": { sts: 12,  sek: 2300, scb: 700, map: 2 },
			"13": { sts: 13,  sek: 3000, scb: 800, map: 2 }
			}
			
var obj_m = {
			sek:ek_msg,
			sts:ts_msg,
			scb:cb_msg,
			sbs:bs_msg,
			map:map_msg,
			} 

signal objective_completed
var objective_msg

func _ready():
	global.connect("enemy_killed",self,"verify_objective")
	global.connect("combo",self,"verify_objective")
	global.connect("minute_changed",self,"verify_objective")
	pass
	
func verify_objective():
	check_objective(sek,global.enemies_killed)
	check_objective(sts,int(global.time_spent/60))
	check_objective(scb,global.combo)
	check_objective(sbs,global.boss_killed)
	
func get_actual_objective():
	return obj[str(global.level)]
	
func check_objective(nobj,vcomp):
	var o = get_actual_objective()
	if o.has(nobj) and (! global.completed_objectives.has(nobj)):
		var v = o[nobj]
		if vcomp == v:
			if o.has(map) and (global.map_selected != o[map]):
				return
			objective_msg = obj_m[nobj] % v
			global.completed_objectives.append(nobj)
			if ((o.size() - global.completed_objectives.size()) == 1) and (o.has(map)):
				global.completed_objectives.append(map)
			emit_signal("objective_completed")
	
