extends Node
	
const file_path = "user://savegame.save"
	
func _ready():
	pass
	
func save_game():
	var save_game = File.new()
	save_game.open(file_path, File.WRITE)
	var node_data = save_dict()
	save_game.store_line(to_json(node_data))
	save_game.close()
	
func load_game():
	var save_game = File.new()
	if not save_game.file_exists(file_path):
		return # Error! We don't have a save to load.
	
	# Load the file line by line and process that dictionary to restore the object it represents
	save_game.open(file_path, File.READ)
	var current_line = parse_json(save_game.get_line())
	global.PLAYER_NAME = valuedef(current_line,"player_name","")
	global.gun_equipped = current_line["gun_equipped"]
	global.map_selected = valuedef(current_line,"map_selected",0)
	global.level = valuedef(current_line,"level",1)
	global.completed_objectives = valuedef(current_line,"completed_objectives",[])
	global.guns_acquired = current_line["guns_acquired"]
	global.wallet = current_line["wallet"]
	global.enemies_killed_general = current_line["enemies_killed_general"]
	global.enemies_killed_best = current_line["enemies_killed_best"]
	global.time_spent_general = current_line["time_spent_general"]
	global.time_spent_best = current_line["time_spent_best"]
	global.combo_best = current_line["combo_best"]
	global.damage_deal_general = current_line["damage_deal_general"]
	global.damage_deal_best = current_line["damage_deal_best"]
	global.damage_taken_general = current_line["damage_taken_general"]
	global.damage_taken_best = current_line["damage_taken_best"]
	global.boss_killed_general = valuedef(current_line,"boss_killed_general",0)
	global.boss_killed_best = valuedef(current_line,"boss_killed_best",0)
	global.show_virtual_buttons = valuedef(current_line,"show_virtual_buttons",true)
	
	save_game.close()
	
func save_dict():
	var save_dict = {
		"player_name" : global.PLAYER_NAME ,
		"gun_equipped" : global.gun_equipped ,
		"map_selected" : global.map_selected ,
		"level" : global.level ,
		"completed_objectives" : global.completed_objectives ,
		"guns_acquired" : global.guns_acquired,
		"wallet" : global.wallet,
		"enemies_killed_general" : global.enemies_killed_general ,
		"enemies_killed_best" : global.enemies_killed_best,
		"time_spent_general" : global.time_spent_general ,
		"time_spent_best" : global.time_spent_best ,
		"combo_best" : global.combo_best ,
		"damage_deal_general" : global.damage_deal_general ,
		"damage_deal_best" : global.damage_deal_best ,
		"damage_taken_general" : global.damage_taken_general ,
		"damage_taken_best" : global.damage_taken_best ,
		"boss_killed_general" : global.boss_killed_general ,
		"boss_killed_best" : global.boss_killed_best ,
		"show_virtual_buttons" : global.show_virtual_buttons 
	}
	return save_dict
	
func valuedef(cl,key,def):
	if cl.has(key):
		return cl[key]
	else:
		return def
	
	
	



