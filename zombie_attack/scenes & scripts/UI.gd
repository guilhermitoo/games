extends CanvasLayer

#onready var lbltime = get_node("lbl_time")
onready var lblmun = get_node("pb_cd_reload/lbl_mun")

var _min = 0
var _sec = 0
var smin = "00"
var ssec = "00"
var android = false

func _ready():
	global.connect("hp_changed",self,"show_hp")
	global.connect("mun_changed",self,"show_mun")
	global.connect("combo",self,"combo")
	objectives.connect("objective_completed",self,"show_objective")
	global.connect("gun_interval_changed",self,"start_cd_shoot")
	global.connect("reload_interval_changed",self,"start_cd_reload")
	global.connect("enemy_killed",self,"count_kill")
	android = ( global.control_type == 1 )
	if android:
		if global.show_virtual_buttons:
			$ab_direction.show()
			$ab_shoot.show()
			$right/fire.show()
		else:
			$ab_direction.hide()
			$ab_shoot.hide()
			$right/fire.hide()
		$ab_shoot.realocateX($right.rect_position.x -133)
	else:
		$ab_direction.hide()
		$ab_shoot.hide()
		$right/fire.hide()
	show_pause_panel()
	#prepare_proportion()
	set_process(true)
	pass

func count_kill():
	$Container/lbl_kills.text = "kills: "+str(global.enemies_killed)

func combo():
	$lbl_combo.visible = (global.combo > 1)
	$lbl_combo.text = "Combo %d!" % global.combo
	$anim.stop(true)
	$anim.play("combo")

func _process(delta):
	if !get_tree().paused:
		global.time_spent += delta
		show_time()
	if Input.is_action_just_pressed("pause"):
		pause()
	
func show_time():
	_min = int(int(global.time_spent)/60)
	_sec = int(global.time_spent) % 60
	if _min < 10:
		smin = "0"+str(_min)
	else:
		smin = str(_min)
	if _sec < 10:
		ssec = "0"+str(_sec)
	else:
		ssec = str(_sec)
	$Container/lbl_tempo.text = smin+":"+ssec
	
func show_hp():
	if $pb_hp.max_value == 999999:
		$pb_hp.max_value = global.player_hp
	$pb_hp.value = global.player_hp
	pass
	
func show_mun():
	lblmun.text = str(global.player_actual_mun)+"/"+str(global.player_max_mun)
	
func pause():
	get_tree().paused = !get_tree().paused
	show_pause_panel()
	
func show_pause_panel():
	$pause/pn_pause.visible = get_tree().paused
	$right/btn_pause.visible = !get_tree().paused and (global.control_type == 1)
	
func _on_btn_sair_pressed():
	global.clicksound()
	loading.goto_scene("res://scenes & scripts/preparation_screen.tscn")
	pause()
	
func _on_TouchScreenButton_pressed():
	global.clicksound()
	pause()
	pass # replace with function body

func _on_anim_animation_finished(anim_name):
	if anim_name == "combo":
		global.combo = 0

func show_objective():
	$lbl_objetivo.text = objectives.objective_msg
	$anim_obj.play("show_objective")
	yield(get_tree().create_timer(8,false),"timeout")
	$anim_obj.play_backwards("show_objective")
	
func _on_btn_retomar_pressed():
	global.clicksound()
	pause()
	pass # replace with function body

func _on_Timer_timeout():
	if $pb_cd_shoot.value > 0:
		$pb_cd_shoot.value -= $pb_cd_shoot_timer.wait_time
	pass # replace with function body
	
func start_cd_shoot():
	$pb_cd_shoot.max_value = global.gun_interval
	$pb_cd_shoot.step = $pb_cd_shoot_timer.wait_time
	$pb_cd_shoot.value = global.gun_interval
	$pb_cd_shoot_timer.start()
	
func start_cd_reload():
	$pb_cd_reload.max_value = global.reload_interval
	$pb_cd_reload.step = $pb_cd_reload_timer.wait_time
	$pb_cd_reload.value = global.reload_interval
	$pb_cd_reload_timer.start()
	
func _on_pb_cd_reload_timer_timeout():
	if $pb_cd_reload.value > 0:
		$pb_cd_reload.value -= $pb_cd_reload_timer.wait_time
	pass # replace with function body

func prepare_proportion():
	var prop = global.get_proportion()
	if prop == 2:
		$right/btn_pause.position.x = 142
		$right/fire.position.x = 710 + 142
	else:
		$right/btn_pause.position.x = 0
		$right/fire.position.x = 710
	pass
