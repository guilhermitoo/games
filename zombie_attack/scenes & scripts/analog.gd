extends Sprite

const RADIUS = 85
const SMALL_RADIUS = 43
const MAX_SPEED = RADIUS-SMALL_RADIUS

var stick_pos
var evt_index = -1
onready var an = get_node("../")
onready var sm = get_node("small")

func _init():
	stick_pos = position
	
func _ready():
	an.stick_vector = Vector2()
	an.stick_angle = 0
	an.stick_speed = 0
	sm.position = Vector2()

func _input(event):
	stick_pos = position
	if event is InputEventScreenTouch:
		if event.is_pressed():
			if stick_pos.distance_to(event.position) < RADIUS:
				evt_index = event.index
		elif evt_index != -1:
			if evt_index == event.index:
				evt_index = -1
				sm.position = Vector2()
				an.stick_vector = Vector2()
				an.stick_angle = 0
				an.stick_speed = 0

	if evt_index != -1 and event is InputEventScreenDrag and evt_index == event.index:
		var dist = stick_pos.distance_to(event.position)

		if dist + SMALL_RADIUS > RADIUS:
			dist = MAX_SPEED

		var vect = (event.position - stick_pos).normalized()

		var ang = event.position.angle_to_point(stick_pos)

		an.stick_vector = vect
		an.stick_angle = ang
		an.stick_speed = dist

		sm.position = vect * dist
#	elif evt_index != -1 and event is InputEventScreenTouch and evt_index == event.index:

