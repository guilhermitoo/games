extends Node2D

var stick_speed = 0
var stick_angle = 0
var stick_vector = Vector2()

func hide():
	$analog.hide()
	
func show():
	$analog.show()

func realocateX(pos_x):
	$analog.position.x = pos_x
