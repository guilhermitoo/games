extends Node

export(int) var time = 0 
export(int) var enemies = 0
export(int) var combo = 0
var maxtime = 0
var maxenemies = 0
var maxcombo = 0

onready var lbltime = get_node("lbl_time")
onready var lblenemies = get_node("lbl_enemies")
onready var lblcombo = get_node("lbl_combo")
onready var animation = get_node("ap")

func _ready():
	maxtime = global.time_spent
	maxenemies = global.enemies_killed
	maxcombo = global.maxcombo_game
	$btn_concluir.disabled = true
	
	prepare_anim()
	
	set_process(true)
	
	animation.play("anim")
	pass

func _process(delta):
	lbltime.text = global.format_time(time,global.TimeFormat.FORMAT_MINUTES | global.TimeFormat.FORMAT_SECONDS)
	lblenemies.text = str(enemies)
	lblcombo.text = str(combo)
	pass
	
func calculate_values():
	#calcula os valores
	var money = 0
	
	var TS = maxtime # Tempo de Sobrevivência ( em segundos. caso sobreviveu 5:45 = 345)
	var AB = maxenemies # Abates (número de abates)
	var DC = global.damage_deal # Dano Causado
	var DS = global.damage_taken # Dano Sofrido
	var B  = 0 # Bônus Pegos
	var BP = 0 # Bônus Perdidos
	var MC = maxcombo # Maior Combo
	var DM = global.map_difficulty # Dificuldade do Mapa
	
	money = (((TS/2.5) * DM) + ((DC-DS)) + (B-BP) + (MC*3) + (AB*DM) ) * 1.5
	money = int(money)
	if money < 0:
		money = 0
	
	$pnl_bottom/lbl_total.text = global.format_float(money)
	
	global.finish_score()
	
	$btn_concluir.disabled = false
	
	if money > 0:
		$top_menu/title/wallet.receive(money)
	
	save_game.save_game()
	pass
	
func prepare_anim():
	animation.get_animation("anim").track_insert_key(0,1.4,maxtime)
	animation.get_animation("anim").track_insert_key(1,2.1,maxenemies)
	animation.get_animation("anim").track_insert_key(2,2.8,maxcombo)
	
func _on_ap_animation_finished(anim_name):
	calculate_values()
	
func _on_btn_concluir_pressed():
	loading.goto_scene("res://scenes & scripts/preparation_screen.tscn")
	pass # replace with function body
