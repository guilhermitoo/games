extends Panel

onready var edtNome = $Panel/edtNome

func _ready():
	check_name()

func check_name():
	if global.PLAYER_NAME == "":
		edtNome.grab_focus()
		show()

func _on_btnConfirmarNome_pressed():
	var _name = edtNome.text
	_name = _name.trim_prefix("")
	_name = _name.trim_suffix("")
	if str(_name).length() > 0:
		global.PLAYER_NAME = _name
		save_game.save_game()
		hide()
