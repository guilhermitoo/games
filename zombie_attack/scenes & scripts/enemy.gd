extends KinematicBody2D

var speed = 90
var speed_var_top = 135
var speed_var_bot = 90
var velocity = Vector2.ZERO
var path = []
var threshold = 16
var w_speed = 42
var hp = 10
var hp_top = 12
var hp_bot = 5
var speed_inc = 1.60
var init_speed = 0
var damage = 2.5
var damage_inc = 0.08
var alive = true

var _body

onready var target = get_node("../player")
onready var blood = get_node("zombie1_hold/blood")

func _ready():
	randomize()
	init_speed = rand_range(speed_var_bot,speed_var_top)
	speed = init_speed
	randomize()
	#hp = rand_range(hp_bot,hp_top)
	set_physics_process(true)
	add_to_group(global.GROUP_ENEMY)
	$Area2D.add_to_group(global.GROUP_ENEMY)
	global.enemy_alive += 1
	pass
	
func reset_speed():
	speed = init_speed
	
func _physics_process(delta):
	if path.size() > 0:
		move_to_target()
	#move_zombie(delta)
	
func move_to_target():
	if !alive:
		return
	if global_position.distance_to(path[0]) < threshold:
		path.remove(0)
	else:
		var direction = global_position.direction_to(path[0])
		look_at(position+direction)
		velocity = direction * speed
		velocity = move_and_slide(velocity)
		
func get_target_path(target_pos):
		path = global.get_main().map.get_simple_path(position,target_pos, true)
	
func move_zombie(delta):
	if (target != null) and (hp > 0):
		look_at(target.position)
		
		var dir = (target.position - position).normalized()
		var motion = dir * speed * w_speed * delta
		
		move_and_slide(motion)

func take_damage(damage):
	if alive:
		if damage > hp:
			damage = hp
		hp -= damage
		blood.emitting = true
		global.damage_deal += damage
		go_back()
		print("levou dano")
		reset_speed()
		if hp <= 0:
			kill()
		
func kill():
	if alive:
		alive = false
		collision_layer = 0
		collision_mask = 0
		$Area2D.collision_layer = 0
		$Area2D.collision_mask = 0
		global.enemy_alive -= 1
		global.set_enemies_killed(global.enemies_killed+1)
		$AnimationPlayer.play("death")
		$Timer.stop()
		var bld = global.preblood.instance()
		bld.position = position
		bld.rotation_degrees = rotation_degrees
		global.get_main().add_child(bld)

func _on_Timer_timeout():
	# a cada segundo aumenta a velocidade e dano do zumbi
	speed += speed_inc
	damage += damage_inc
	if hp > 0:
		$Timer.start()
	pass # replace with function body

func _on_Area2D_body_entered(body):
	if body.is_in_group(global.GROUP_PLAYER) or body.is_in_group(global.GROUP_MAP_OBJECT):
		_body = body
		# causa dano ao contato e inicia o timer
		# para continuar dando dano se o contato se manter
		if $tmDamage.time_left == 0:
			deal_damage(_body)
			$tmDamage.start()

func deal_damage(body):
	if body == null:
		return false
		
	if body.has_method("take_damage"):
		go_back()
		body.take_damage(damage)
		return true
	else:
		return false

func go_back():
	$AnimationPlayer.play("hit")

func _on_tmDamage_timeout():
	if ! deal_damage(_body):
		$tmDamage.stop()
	else:
		$tmDamage.start()
	pass # replace with function body

func _on_Area2D_body_exited(body):
	if _body == body:
		_body = null
	pass # replace with function body


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "death":
		queue_free()
	pass # Replace with function body.


func _on_Timer2_timeout():
	get_target_path(target.position)
	pass # Replace with function body.
