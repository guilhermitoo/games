extends Node2D

var pre_enemy = preload("res://scenes & scripts/enemy.tscn")
onready var difficulty = owner.difficulty

var difficulty_fat = 1.5
var time_to_resp = 5.2

var cumulative_diff = 0
var percentual_cumulative = 0.008

var time_horda = 3.5

var time_control = 0

var generating = true

var limit = 40
var max_limit = 53

func _ready():
	start_timer()
	set_process(true)
	time_control = 7
	pass

func _process(delta):
	if time_control <= 0:
		generating = !generating
		time_control = time_horda
		if limit < max_limit:
			limit += 1
	else:
		time_control -= delta

func start_timer():
	cumulative_diff += $Timer.wait_time * percentual_cumulative
	randomize()
	$Timer.wait_time = time_to_resp - (difficulty * difficulty_fat) - (rand_range(0,0.5)) - cumulative_diff
	$Timer.start()

func spawn_enemy():
	if (!generating) or (global.enemy_alive >= limit):
		return 
		
	var en = pre_enemy.instance()
	randomize()
	var rand_pos = rand_range(-50,50)
	en.hp += cumulative_diff
	en.speed += cumulative_diff
	en.position = position
	en.position.y += rand_pos
	en.position.x += rand_pos
	global.get_main().add_child(en)
	pass

func _on_Timer_timeout():
	spawn_enemy()
	start_timer()

