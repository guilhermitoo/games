extends Label

func _ready():
	visible = global.show_fps
	$Timer.start()
	
func _on_Timer_timeout():
	$".".set_text("fps: "+str(Engine.get_frames_per_second()))
	$Timer.start()
