extends Node2D

var map = null

func _ready():
	# garante que os scores foram finalizados e zerados
	global.finish_score()
	
	global.set_player_hp(global.player_default_hp)
	
	$UI.show_mun()
	$UI.show_hp()
	
	map = global.maps[global.map_selected].instance()
	add_child(map)
	
	$player.position = map.spawner_position
	
	global.map_difficulty = map.difficulty
	
	
	#var prop = global.get_proportion()
	#if prop == 2:
	#	$ab_shoot/analog.position.x = 1145
	#else:
	#	$ab_shoot/analog.position.x = 1003
		
	#emit_signal("ready")
	


func _on_AudioStreamPlayer2D_finished():
	$player/AudioStreamPlayer2D.play(0)
	pass # Replace with function body.
