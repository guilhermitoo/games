extends Area2D

const LITTLE = 0
const BIG = 1

const STROKE = 0
const BURST = 1
const CONE = 2

enum e_weapon_type {LITTLE,BIG}
enum e_shoot_type {STROKE,BURST,CONE}
var pre_mun = preload("res://scenes & scripts/mun.tscn")
export(float,1,20) var damage = 1
export(float,1,20) var munition = 2
export(float,1,5) var munition_fat = 3
export(float,1,20) var speed = 1
export(int,1,20) var _range = 5
export(float,0,5) var load_time = 2
export(e_weapon_type) var weapon_type 
export(e_shoot_type) var shoot_type
export var description = ""
export var menu_image_path = ""
export(float) var price = 100
export(int) var burst_fat = 5
export(int) var level = 1

var mun_count = 0
var reload = false

var _range_fat = 40 #distancia por pixel multiplicada pelo alcance.
var speed_fat = 0.09
var fire_fat = 0.07

var fire_time = 0

var camera = null

#propriedades de controle
var interval = 0
var interval_burst = 0

var bursting = false

signal munition_off
signal munition_on

func _ready():
	set_process(true)
	reloaded()
	global.set_player_max_mun(mun_count)
	$partic_fire.position = $bullet_leave.position+Vector2(4,0)
	pass
	
func start_reload():
	if mun_count <= 0:
		interval_burst = 0
		emit_signal("munition_off")
		reload = true
		global.reload_interval = load_time
		$loading_timer.start()
	
func reloaded():
	mun_count = munition*munition_fat
	global.set_player_actual_mun(mun_count)
	reload = false
	$loading_timer.stop()
	$loading_timer.wait_time = load_time
	emit_signal("munition_on")
	
func _process(delta):
	if global.control_type == 2:
		if global.is_shooting():
			shoot()
		
	if interval_burst > 0:
		if stroke():
			interval_burst -= 1
			
			if interval_burst <= 0:
				interval = 0.43 #intervalo entre cada rajada(burst)
	else:
		bursting = false
	
	interval -= delta
	
	if Input.is_action_pressed("reload"):
		manual_reload()
	
	if fire_time >= 0:
		fire_time -= delta
	
	if fire_time < 0 and $fire.visible:
		$fire.hide()
	
func manual_reload():
	if !reload:
		mun_count = 0
		Input.action_release("reload")
		start_reload()
	
func shoot():
	if (shoot_type == e_shoot_type.BURST): 
		if (interval_burst <= 0) and (interval <= 0) and !reload:
			bursting = true
			interval_burst = burst_fat
			burst_play()
	elif (shoot_type == e_shoot_type.CONE):
		stroke_cone()
	else:
		stroke()
	
	
func stroke_cone():
	var ret = false
	
	if (interval <= 0) and !reload:
		
		$partic_fire.show()
		$partic_fire.emitting = false
		$partic_fire.emitting = true
		$partic_timer.start()
		
		var e = owner.global_rotation
		var c = $bullet_leave.position
		c = c.rotated(e)
		var b = position
		b = b.rotated(e)
		c = c+ owner.position +b
		
		mun_count -= 1
		global.set_player_actual_mun(mun_count)
		interval = 1.5-(speed*speed_fat)
		global.gun_interval = interval
		
		var count_mun = burst_fat
		for x in range(0,count_mun):
			var mun = pre_mun.instance()
			if x!=0:
				mun.illuminate = false
			mun.damage = damage
			mun._range = _range * _range_fat
			var y = ((count_mun/2.0)-x)/5.0
			var d = Vector2(2,y).rotated(e)
			mun.cross = true
			mun._start(c,d)
			global.get_main().add_child(mun)
		start_reload()
		stroke_play()
		ret = true
	return ret
	
func stroke():
	var ret = false
	
	if (interval <= 0) and !reload:
		
		if has_method("on_stroke"):
			self.call("on_stroke")
		
		$partic_fire.show()
		$partic_fire.emitting = false
		$partic_fire.emitting = true
		$partic_timer.start()
		
		var e = owner.global_rotation
		var d = Vector2(1,0).rotated(e)
		var c = $bullet_leave.position
		c = c.rotated(e)
		var b = position
		b = b.rotated(e)
		
		mun_count -= 1
		global.set_player_actual_mun(mun_count)
		interval = 1.5-(speed*speed_fat)
		global.gun_interval = interval
		var mun = pre_mun.instance()
		mun.damage = damage
		mun._range = _range * _range_fat
		c = c+ owner.position +b
		mun._start(c,d)
		global.get_main().add_child(mun)
		start_reload()
		if (shoot_type == e_shoot_type.STROKE):
			stroke_play()
		ret = true
	return ret
	
func stroke_play():
	var s = AudioStreamPlayer.new()
	s.connect("finished",self,"_on_sound_finished",[s])
	s.stream = $sound.stream
	s.pitch_scale = $sound.pitch_scale	
	global.get_main().add_child(s)
	s.play()
	if camera != null:
		if camera.has_method("shake"):
			camera.shake(0.2,10,1)
	
func burst_play():
	var s = AudioStreamPlayer.new()
	s.connect("finished",self,"_on_sound_finished",[s])
	s.stream = $sound_burst.stream
	s.pitch_scale = $sound_burst.pitch_scale
	global.get_main().add_child(s)
	s.play()
	
	
func _on_loading_timer_timeout():
	reloaded()
	pass # replace with function body
	
func _on_sound_finished(s):
	#finaliza o som
	s.queue_free()
	pass # replace with function body


func _on_partic_timer_timeout():
	$partic_fire.hide()
	pass # replace with function body
