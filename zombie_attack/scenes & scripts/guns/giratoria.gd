extends "res://scenes & scripts/gun.gd"

var max_shooting_speed = 15.80
var min_shooting_speed = 10

func _ready():
	pass # Replace with function body.

func _on_Timer_timeout():
	# cada vez que passar aqui verifica se a arma está atirando.
	# SE SIM:
	# aumenta a velocidade, a um máximo de 16.25
	# SE NAO:
	# diminui a velocidade, a um máximo de 10
	if global.is_shooting():
		if speed < max_shooting_speed:
			speed += 0.20
	else:
		if speed > min_shooting_speed:
			speed -= 0.20
	pass # Replace with function body.

func on_stroke():
	if $asprite.frame < 2:
		$asprite.frame += 1
	else:
		$asprite.frame = 0
