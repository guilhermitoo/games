extends Node

onready var list = get_node("list")

func _ready():
	set_process(true)
	list.prepare(global.guns,global.gun_equipped)
	
func show_buttons():
	# verifica se deve ou não mostrar o botão de equipar.
	# deve mostrar apenas para armas adiquiridas
	var ind = list.get_active_item().index
	var ga = global.guns_acquired
	var acquired = ga.has(ind) #find(ind)
	var eqquiped = global.gun_equipped == ind
	$btn_selecionar.visible = acquired and !eqquiped
	$btn_comprar.visible = (!acquired) and (!list.get_active_item().blocked)
	pass
	
func _on_btn_comprar_pressed():
	global.clicksound()
	buy()
	pass # replace with function body
	
func buy():
	if list.get_active_item().buy():
		$top_menu/title/wallet.pay(list.get_active_item()._gun.price)
		show_buttons()
	
func _on_btn_voltar_pressed():
	global.clicksound()
	voltar()
	pass # replace with function body
	
func voltar():
	loading.goto_scene("res://scenes & scripts/preparation_screen.tscn")
	
func _on_list_active_item_changed():
	show_buttons()
	pass # replace with function body


func _on_btn_selecionar_pressed():
	global.reloadsound()
	global.gun_equipped = list.get_active_item().index
	voltar()
