extends Panel

var index = 0
var highlight = false setget set_highlight
var _gun
var blocked = false

func _ready():
	pass

func _load(gun):
	if (gun.shoot_type == gun.e_shoot_type.CONE):
		$content/bar_damage.value = gun.damage * gun.burst_fat
	else:
		$content/bar_damage.value = gun.damage
	$content/bar_munition.value = gun.munition
	$content/bar_speed.value = gun.speed
	$content/bar_range.value = gun._range
	$content/name.text = gun.description
	$content/image.set_texture(load(gun.menu_image_path))
	$content/price.text = global.format_float(gun.price)
	$content/bloq.text = "liberado no nível %d" % gun.level
	
	_gun = gun
	
	update_ui()
	
func set_highlight(value):
	highlight = value
#	if value:
#		$content/cr_gray.hide()
#	else:
#		$content/cr_gray.show()
	
func buy():
	if _gun.price <= global.wallet:
		global.guns_acquired.append(index)
		update_ui()
		return true
	else:
		return false
	
func update_ui():
	var acquired = global.guns_acquired.has(index)
	var eqquiped = (global.gun_equipped == index)
	
	blocked = _gun.level > global.level
	
	$content/price.visible = !(acquired or blocked)
	$content/coin2.visible = !(acquired or blocked)
	$content/cr_block.visible = !acquired
	
	$content/eqquiped.visible = eqquiped
	
	$content/bloq.visible = blocked
	$content/cadeado.visible = blocked
	
	
	
	
