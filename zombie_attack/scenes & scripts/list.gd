extends Panel

export(String) var item_path = ""
export(int) var item_width = 200
export(int) var item_height = 200

var pre_item
var item_index = 0
var active_index = 0
var transiting = false

onready var list = get_node("scroll/list_h")
onready var sc = get_node("scroll")

signal active_item_changed

func _ready():
	set_process(true)
	pass
	
func prepare(itens,focused):
	pre_item = load(item_path)
	
	for i in range(0,itens.size()):
		add_item(itens[i].instance(),itens)
	
	go_to_item(focused)
	
func add_item(item,itens):
	if (item_index == 0):
		list.add_child(get_empty_control())

	var it = pre_item.instance()
	it.rect_min_size = Vector2(item_width-4,item_height)
	it.index = item_index
	item_index += 1
	if it.has_method("_load"):
		it._load(item)
		
	list.add_child(it)
	
	if (item_index == itens.size()):
		list.add_child(get_empty_control())
	
func get_empty_control():
	var c = Control.new()
	var prop = global.get_proportion()
	
	if prop == 2:
		c.rect_min_size = Vector2((sc.rect_size.x-item_width)/2-4,400)
	else:
		c.rect_min_size = Vector2((sc.rect_size.x-item_width)/2-4,200)
		
	c.rect_size.x = c.rect_min_size.x
	c.rect_size.y = c.rect_min_size.y
	return c
	
func go_to_item(internal_index): #internal_index seria o indice interno do item. para atribuir para o active item, deve somar 1
	if transiting:
		return
	transiting = true
	set_active(internal_index+1)
	$ap.get_animation("next").add_track(0)
	$ap.get_animation("next").step = 0.1
	$ap.get_animation("next").track_set_path(0,"scroll:scroll_horizontal")
	$ap.get_animation("next").track_insert_key(0,0,sc.scroll_horizontal,0.5)
	$ap.get_animation("next").track_insert_key(0,0.2,sc.scroll_horizontal+(item_width*internal_index),2)
	$ap.play("next")
	
func go_to_next():
	if transiting:
		return
	transiting = true
	if (active_index < list.get_child_count()-1): #and (sc.scroll_horizontal+item_width):
		$ap.get_animation("next").add_track(0)
		$ap.get_animation("next").step = 0.1
		$ap.get_animation("next").track_set_path(0,"scroll:scroll_horizontal")
		$ap.get_animation("next").track_insert_key(0,0,sc.scroll_horizontal,0.5)
		$ap.get_animation("next").track_insert_key(0,0.2,sc.scroll_horizontal+item_width,2)
		$ap.play("next")
		update_active_item(1)
	
func back_to_previous():
	if transiting:
		return
	transiting = true
	if (active_index > 0): # and (sc.scroll_horizontal-item_width>0):
		$ap.get_animation("previous").add_track(0)
		$ap.get_animation("previous").step = 0.1
		$ap.get_animation("previous").track_set_path(0,"scroll:scroll_horizontal")
		$ap.get_animation("previous").track_insert_key(0,0,sc.scroll_horizontal,0.5)
		$ap.get_animation("previous").track_insert_key(0,0.2,sc.scroll_horizontal-item_width,2)
		$ap.play("previous")
		update_active_item(-1)
	
func do_highlight():
	var i = list.get_child(active_index)
	if i.has_method("set_highlight"):
		i.set_highlight(true)
	
func remove_highlight():
	var i = list.get_child(active_index)
	if i.has_method("set_highlight"):
		i.set_highlight(false)
		
func update_active_item(value):
	remove_highlight()
	set_active(active_index + value)
	
func set_active(index):
	if list.get_child(index).has_method("set_highlight"):
		active_index = index
	else:
		if index==0:
			active_index = index+1
	do_highlight()
	emit_signal("active_item_changed")
	update()
	
func get_active_item():
	return list.get_child(active_index)
	
func _on_btn_left_pressed():
	back_to_previous()
	pass # replace with function body
	
func _on_btn_right_pressed():
	go_to_next()
	pass # replace with function body
	
func _on_ap_animation_finished(anim_name):
	transiting = false
	pass # replace with function body
