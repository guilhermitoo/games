extends Navigation2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var menu_image_path = ""
export var description = ""
export var level = 0
export var difficulty = 1
var spawner_position = Vector2(0,0)


# Called when the node enters the scene tree for the first time.
func _ready():
	if has_node("spawn_point"):
		spawner_position = get_node("spawn_point").position
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
