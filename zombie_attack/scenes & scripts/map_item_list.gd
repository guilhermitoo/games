extends Panel

var index = 0
var highlight = false setget set_highlight
var _map
var blocked = false

func _ready():
	pass

func _load(map):
	$content/image/TextureRect.set_texture(load(map.menu_image_path))
	$content/name.text = map.description
	$content/bloq.text = "liberado no nível %d" % map.level
	
	_map = map
	
	update_ui()
	
func set_highlight(value):
	highlight = value
	
func update_ui():
	var selected = (global.map_selected == index)
	
	blocked = _map.level > global.level
	
	$content/cr_block.visible = blocked
	
	$content/eqquiped.visible = selected
	
	$content/bloq.visible = blocked
	$content/cadeado.visible = blocked
	
	
	
	
