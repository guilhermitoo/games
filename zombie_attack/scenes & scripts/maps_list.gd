extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)
	$list.prepare(global.maps,global.map_selected)

func show_buttons():
	# verifica se deve ou não mostrar o botão de equipar.
	# deve mostrar apenas para armas adiquiridas
	var ind = $list.get_active_item().index
	var selected = global.map_selected == ind
	var blocked  = $list.get_active_item().blocked
	$btn_selecionar.visible = !selected and !blocked
	pass

func _on_btn_voltar_pressed():
	global.clicksound()
	voltar()
	
func voltar():
	loading.goto_scene("res://scenes & scripts/preparation_screen.tscn")


func _on_btn_selecionar_pressed():
	global.clicksound()
	global.map_selected = $list.get_active_item().index
	voltar()


func _on_list_active_item_changed():
	show_buttons()
