extends RigidBody2D

var damage = 10
var speed = 1000

var velocity = Vector2(0,0)
var _range = 100
var init_pos = Vector2()
var affected = false
var duration = 1 #seconds
var illuminate = true

var cross : bool = false
var light_time = 0.05

var interacted_with : Array = []

func _start(_position,_direction):
	if !illuminate:
		$light.hide()
	position = _position
	init_pos = _position
	velocity = _direction.normalized() * speed
	rotation = _direction.angle()
	add_to_group(global.GROUP_MUNITION)
	set_process(true)
	apply_impulse(velocity,velocity)
	self.show()
	pass

func _ready():
	pass
	
func _process(delta):
	light_time -= delta
	duration -= delta
	if light_time < 0:
		$light.hide()
	if position.distance_to(init_pos) > _range:
		free_instance()
	if duration <= 0:
		free_instance()
	
	
func body_enter( area ):
	if !affected:
		if (not area.is_in_group(global.GROUP_PLAYER)) and (not area.is_in_group(global.GROUP_MUNITION)): 
	#	if area.is_in_group(global.GROUP_ENEMY) or area.is_in_group(global.GROUP_MAP_OBJECT):
			if area.has_method("take_damage"):
				if !cross:
					affected = true
				area.take_damage(damage)
			if !cross:
				free_instance()
	
func free_instance():
	queue_free()
	
func _on_Area2D_body_entered(body):
	body_enter(body)
