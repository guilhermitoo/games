extends Panel

var desc_obj = ""

func _ready():
	objectives.connect("objective_completed",self,"_update_obj")
	pass
	
func _load(i):
	desc_obj = i
	_update_obj()
	
func _update_obj():
	if desc_obj == "":
		return
	
	var obj = objectives.get_actual_objective()
	var text = ""
	if desc_obj == objectives.map:
		var map = global.maps[obj.map].instance()
		text = objectives.obj_m[desc_obj] % map.description
		map.queue_free()
	else:
		text = objectives.obj_m[desc_obj] % obj[desc_obj]
		
	var done = global.completed_objectives.has(desc_obj)
	
	$label.text = text
	$label/control/icon.visible = done
	
	if done:
		modulate.a8 = 255
	else:
		modulate.a8 = 80







