extends Panel

var pre_item = preload("res://scenes & scripts/objective_item.tscn")

func _ready():
	# percorre a lista de objetivos e carrega eles na tela.
	# Os já concluídos, exibir com o certo na frente.
	# Os não concluídos, exibir um pouco apagados
	prepare()
	
	pass

func add(i):
	var item = pre_item.instance()
	item.rect_min_size = Vector2(487,36)
	item._load(i)
	$list.add_child(item)
	pass
	
func prepare():
	for x in range(0, $list.get_child_count()):
		$list.get_child(x).queue_free()
	
	var obj = objectives.get_actual_objective()
	# percorre os objetivos daquele nível e adiciona na lista
	for i in obj.keys():
		add(i)
		
	#$lbl_lvl.text = "Nível %d"
	#$lbl_lvl.text = $lbl_lvl.text % global.level
	
	$lbl_lvl/emblems.region_rect.position.x = (global.level * 32) -32
	
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
