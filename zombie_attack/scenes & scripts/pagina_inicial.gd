extends CanvasLayer
	
func _ready():
	pass
	
func _on_btnJogar_pressed():
	save_game.load_game()
	global.clicksound()
	loading.goto_scene("res://scenes & scripts/preparation_screen.tscn")
	#get_tree().change_scene("res://scenes & scripts/preparation_screen.tscn")

func _on_btnStatus_pressed():
	global.clicksound()
	loading.goto_scene("res://scenes & scripts/status.tscn")
	pass # replace with function body

func _on_btnCreditos_pressed():
	global.clicksound()
	loading.goto_scene("res://scenes & scripts/credits.tscn")
	pass # Replace with function body.
