extends KinematicBody2D

var target = Vector2(0,0)
var direction = Vector2(0,0)
var speed = 285
var recoil = 130
var rshoot = 1 
var shooting = false
var walking = false
var motion = Vector2(0,0)
var trigger_speed = 0
onready var analog = get_node("../UI/ab_direction")
onready var trigger = get_node("../UI/ab_shoot")
onready var camera = get_node("camera")

var wait_after_shoot = 0
var wait_after_shoot_val = 0.7

var max_hp = 30
var hp = max_hp

func _ready():
	set_physics_process(true)
	define_weapon(global.guns[global.gun_equipped])
	add_to_group(global.GROUP_PLAYER,true)
	global.set_player_hp(hp)
	
	$gun.connect("munition_on",self,"_on_gun_munition_on")
	$gun.connect("munition_off",self,"_on_gun_munition_off")
	pass
	
func define_weapon(pre_weapon):
	var gun = pre_weapon.instance()
	gun.camera = camera
	self.remove_child($gun)
	self.add_child(gun)
	$gun.owner = self
	$range/shape.shape.radius = $gun._range*$gun._range_fat
	define_body()
	
func define_body():
	if self.has_node("gun"):
		var gwt = $gun.weapon_type
		var arm_litte = (gwt == 0)
		
		if arm_litte:
			$gun.position = $hand_pos_little.position
		else:
			$gun.position = $hand_pos_big.position
		$body1.visible = arm_litte
		$body2.visible = !arm_litte
		$arm_body2.visible = !arm_litte
		$body_load.hide()
	
func _physics_process(delta):
	if hp > 0: #se estiver vivo, então faz as outras ações
		move_player(delta)
	
func move_player(delta):
	if global.control_type == 1:
		mobile_movements(delta)
	else:
		keyboard_movements(delta)
	
func keyboard_movements(delta):
	target = global.get_inserted_walk_direction().normalized()
	shooting = global.is_shooting() or $gun.bursting
	direction = get_global_mouse_position()
	
	#if Input.is_action_pressed("instant_fire"):
	#	if instant_trigger():
	#		shooting = true
	#		wait_after_shoot = wait_after_shoot_val
	#else:
	#	shooting = false
		
	walking = (target != Vector2(0,0))
	#var dir_look = direction.normalized()
	do_movement(target,shooting,walking,42,direction,false,delta)
	
func mobile_movements(delta):
	var walking_with_controller : bool = false
	target = global.get_inserted_walk_direction().normalized()
	if target != Vector2.ZERO:
		walking_with_controller = true
	else:
		target = analog.stick_vector.normalized()
	var p = false
	if (trigger.stick_vector == Vector2(0,0)) and (direction != Vector2(0,0)) and (trigger_speed > (42/2)):
		shooting = true
		wait_after_shoot = wait_after_shoot_val
		look_at(direction.normalized()+position)
		$gun.shoot()
	elif trigger.stick_vector != Vector2(0,0):
		look_at(trigger.stick_vector.normalized()+position)
		p = true
	elif $gun.bursting:
		shooting = true
		wait_after_shoot = wait_after_shoot_val
	elif Input.is_action_pressed("instant_fire"):
		if instant_trigger():
			shooting = true
			wait_after_shoot = wait_after_shoot_val
	else:
		shooting = false
	direction = trigger.stick_vector
	trigger_speed = trigger.stick_speed
	walking = (target != Vector2(0,0))
	var speed : int = 42
	if !walking_with_controller:
		speed = analog.stick_speed
	do_movement(target,shooting,walking,speed,null,p,delta)
	
func do_movement(target,shooting,walking,w_speed,dir_look,pointing,delta):
	if shooting or $gun.reload:
		if (dir_look != null):
			look_at(dir_look)
		if shooting:
			rshoot = recoil
		else:
			rshoot = 0
	else:
		rshoot = 0
	
	if walking:
		if !shooting and wait_after_shoot <= 0 and !pointing:
			look_at(target+position)
			
	motion = target * w_speed * (speed-rshoot) * delta
	move_and_slide(motion)
	motion = Vector2(0,0)
	
	if wait_after_shoot > 0:
		wait_after_shoot -= delta
	
func start_reloading():
	if self.has_node("gun"):
		$gun.rotation_degrees = -75
		$gun.position = $hand_pos_big.position
		$body1.hide()
		$body2.hide()
		$arm_body2.hide()
		$body_load.show()
	pass

func finish_reloading():
	if self.has_node("gun"):
		$gun.rotation_degrees = 0
		$body1.region_rect.position = Vector2(0,0)
		define_body()
	pass

func _on_gun_munition_off():
	print("mun_off")
	start_reloading()
	pass # replace with function body

func _on_gun_munition_on():
	finish_reloading()
	pass # replace with function body

func take_damage(damage):
	if damage > hp:
		damage = hp
	hp -= damage
	global.set_player_hp(hp)
	global.damage_taken += damage
	$anim.play("damage")
	if hp <= 0:
		kill()

func kill():
	loading.goto_scene("res://scenes & scripts/conclusion_screen.tscn")

func instant_trigger():
	# percorre a lista de corpos colidindo com $range/shape
	# encontra o mais próximo
	# mira para ele
	# atira
	var dist_to = 1000
	var dist = Vector2(0,0)
	var list = $range.get_overlapping_bodies()
	
	if list.size() <= 0:
		return false
	
	for i in list:
		if i.is_in_group(global.GROUP_ENEMY):
			if self.position.distance_to(i.position) < dist_to:
				dist = i.position
				dist_to = self.position.distance_to(i.position)
			
	
	if dist == Vector2(0,0):
		return false
	
	look_at(dist)
	$gun.shoot()
	return true

func heal():
	hp += 20
	if hp > max_hp:
		hp = max_hp
	global.set_player_hp(hp)
