extends Node

var obj_up_pre = preload("res://scenes & scripts/objective_up.tscn")

func _ready():
	var gun = global.guns[global.gun_equipped].instance()
	var map = global.maps[global.map_selected].instance()
	$selected_gun/btn_select_gun/sprite.set_texture(load(gun.menu_image_path))
	$selected_gun/titulo2.text = gun.description
	$selected_map/sprite.set_texture(load(map.menu_image_path))
	$selected_map/titulo2.text = map.description
	$cbVirtualButton.pressed = global.show_virtual_buttons
	$cbVirtualButton.visible = (global.control_type == 1)
	save_game.save_game()
	pass
	
func _on_btn_select_gun_pressed():
	global.clicksound()
	loading.goto_scene("res://scenes & scripts/guns_list.tscn")
	pass # replace with function body


func _on_btnJogar_pressed():
	global.clicksound()
	loading.goto_scene("res://scenes & scripts/game.tscn",true)
	pass # replace with function body

func _on_tmr_obj_conc_timeout():
	check_level()
	pass # replace with function body

func check_level():
	if global.level == global.last_level:
		return
	
	# se o level foi concluído
	if ( global.completed_objectives.size() == objectives.obj[str(global.level)].size() ):
		var x = obj_up_pre.instance()
		
		self.add_child(x)
		
		var mval = global.level*global.money_per_level
		
		x._start(global.level,mval)
		
		global.completed_objectives = []
		
		global.level += 1
		
		$top_menu/title/wallet.receive(mval)
		
		$objectives_list.prepare()
	
func _on_btn_select_accessorie_pressed():
	global.clicksound()
	#loading.goto_scene("res://scenes & scripts/accessories_list.tscn")
	pass # replace with function body


func _on_btn_select_map_pressed():
	global.clicksound()
	loading.goto_scene("res://scenes & scripts/maps_list.tscn")


func _on_cbVirtualButton_pressed():
	global.show_virtual_buttons = $cbVirtualButton.pressed
