extends Node

onready var enemy_general = get_node("background/data/left/val1")
onready var enemy_best = get_node("background/data/left/val2")
onready var boss_general = get_node("background/data/left/val3")
onready var boss_best = get_node("background/data/left/val4")
onready var time_general = get_node("background/data/left/val5")
onready var time_best = get_node("background/data/left/val6")

onready var combo_best = get_node("background/data/right/val1")
onready var damage_deal_general = get_node("background/data/right/val2")
onready var damage_deal_best = get_node("background/data/right/val3")
onready var damage_taken_general = get_node("background/data/right/val4")
onready var damage_taken_best = get_node("background/data/right/val5")

onready var title = $background/title

func _ready():
	save_game.load_game()
	
	title.text = "Nome: "+global.PLAYER_NAME
	
	$background/lbl7/emblems.region_rect.position.x = (global.level * 32) -32
	
	enemy_general.text = str(global.enemies_killed_general)
	enemy_best.text = str(global.enemies_killed_best)
	boss_general.text = str(global.boss_killed_general)
	boss_best.text = str(global.boss_killed_best)
	time_general.text = global.format_time(global.time_spent_general)
	time_best.text = global.format_time(global.time_spent_best)
	
	combo_best.text = str(global.combo_best)
	damage_deal_general.text = str(int(global.damage_deal_general))
	damage_deal_best.text = str(int(global.damage_deal_best))
	damage_taken_general.text = str(int(global.damage_taken_general))
	damage_taken_best.text = str(int(global.damage_taken_best))
	pass

func _on_btnMenu_pressed():
	loading.goto_scene("res://scenes & scripts/pagina_inicial.tscn")
	pass # replace with function body
