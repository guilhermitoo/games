extends Control

func _ready():
	# coloca casas de milhar no valor inteiro
	global.connect("wallet_changed",self,"update_val")
	update_val()
	pass
	
func pay(value):
	#remove o value da carteira
	global.set_wallet(global.wallet - value)
	$value2.text = global.format_float(value)
	$value2.text = "-"+$value2.text
	$anim.play("transaction")
	
func receive(value):
	global.set_wallet(global.wallet + value)
	$value2.text = global.format_float(value)
	$value2.text = "+"+$value2.text
	$anim.play("transaction")
	
func update_val():
	$value.text = global.format_float(global.wallet)

